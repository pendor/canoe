#ifndef HEADER_CNBEND
#define HEADER_CNBEND

#include "thlib.h"

struct beth_handle_t {
    th_adr              peer;
    void *              offband;
};

struct beth_buf_t {
    uint8 *             data;
    uint32              size;
};

struct beth_io_t {
    dqp                 channel;
    struct beth_buf_t * buf;
    uint32              rd;
    uint32              wr;
};

struct beth_xfer_t {
    xfer                xf;
    struct beth_buf_t * buf;
    uint8               f_source : 1;
    uint8               f_done : 1;
};

#define CN_HANDLE_STRUCTSIZE        sizeof(struct beth_handle_t)
#define CN_HANDLE_STORESIZE         20

typedef struct beth_xfer_t beth_xfer;

typedef struct beth_handle_t *  cn_handle_p;
typedef struct beth_handle_t    cn_handle_t;
typedef struct beth_io_t *      cn_io;
typedef struct beth_xfer_t *    cn_xfer;
typedef struct beth_buf_t *     cn_buf;

void    beth_update         ();
//void    beth_registerdq     (dqp q);
void    beth_init           ();
void    beth_setgrub        (void *addr, uint32 data);

#define cndie                   die
#define cnmsg                   dbgmsgx
#define cnerr                   dbgerr
#define cnlog                   dbglog
#define cn_malloc               mem_malloc
#define cn_free                 mem_free

#endif
