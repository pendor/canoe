#include "thlib.h"
#include "config_bend.h"
#define CANOE_LOGLEVEL  BETH_LOGLEVEL
#include "canoe.h"

#ifndef BETH_LOADER_SLOTS
#define BETH_LOADER_SLOTS 10
#endif
#ifndef BETH_LOADER_BLOCK       //8 byte align
#define BETH_LOADER_BLOCK   800
#endif
#ifndef BETH_HANDLESLOTS
#define BETH_HANDLESLOTS    8
#endif

#ifndef MIN
#define MIN(x, y)   ((x) < (y) ? (x) : (y))
#endif

void *  beth_grub_addr = 0;
uint32  beth_grub_data = 0;

cn_buf cn_buffer(uint32 size, void *data)
{
    if(data != 0)
        cnerr("cannot use data parameter yet\n");
    cn_buf buf = cn_malloc(sizeof(cn_buf) + size);
    buf->data = (uint8 *)(buf + 1);
    buf->size = size;
    return buf;
}

void cn_buffer_del(cn_buf buf)
{
    cn_free(buf);
}

cn_io cn_fifo(uint32 size, uint32 wrap, cn_handle_p local, cn_handle_p remote, uint8 sink)
{
    cn_io io = cn_malloc(sizeof(struct beth_io_t) + sizeof(doubleq) + sizeof(queue) + size + wrap);
    cnlog2("beth: fifo created size:%d+%d @%#x\n", size, wrap, io);
    dqp dq = (dqp)(io + 1);
    qp q = (qp)(dq + 1);
    q_init(q, size, wrap, q + 1);
    cnlog3("beth: fifo dq:@%#x q:@%#x data:@%x\n", dq, q, q + 1);
    dq_init(dq, q);
    io->channel = dq;
    io->buf = 0;
    //connect
    if(!ISALIGN8(size))
        cnerr("beth  : cn_connect on a not 8 byte aligned buffer\n");
    dq_mode mode = sink ? dqm_sink : dqm_source;
    dq_setoffband(dq, mod_ext2int(local->offband), remote->peer, remote->offband, mode, 1);
    //while(dq_connecting(dq))
    //    os_wait();
    //
    return io; 
}

cn_io cn_access(cn_buf buf)
{
    cn_io io = cn_malloc(sizeof(cn_io));
    io->channel  = 0;
    io->buf = buf;
    io->rd = 0;
    io->wr = 0;
    return io;
}

void cn_io_del(cn_io io)
{
    cn_free(io);
}

void * cn_poke(cn_io ff, uint32 size)
{
    if(ff->channel)
        return q_poke(ff->channel->q, size);
    //buffer access
    if(ff->wr + size > ff->buf->size)
        cnerr("beth: buffer too short\n");
    uint8 *p = ff->buf->data + ff->wr;
    ff->wr += size;
    return p;
}

void cn_push(cn_io ff)
{
    if(ff->channel)
        q_push(ff->channel->q);
}

void * cn_peek(cn_io ff, uint32 size)
{
    if(ff->channel)
        return q_peek(ff->channel->q, size);
    //buffer access
    if(ff->rd + size > ff->buf->size)
        cnerr("beth: buffer too short\n");
    if(ff->rd + size > ff->wr)
        return 0;
    uint8 *p = ff->buf->data + ff->wr;
    ff->wr += size;
    return p;
}

void cn_pop(cn_io ff)
{
    if(ff->channel)
        q_pop(ff->channel->q);
}

uint32 cn_avail(cn_io ff)
{
    if(ff->channel)
        return q_avail(ff->channel->q);
    return ff->wr - ff->rd;
}

uint32 cn_space(cn_io ff)
{
    if(ff->channel)
        return q_free(ff->channel->q);
    return ff->buf->size - ff->wr;
}

uint8 cn_locked(cn_io ff)
{
    if(ff->channel)
        return q_locked(ff->channel->q);
    return 0;
}

cn_xfer cn_transfer(cn_buf buf, cn_handle_p local, cn_handle_p remote, uint8 send)
{
    beth_xfer *xf = cn_malloc(sizeof(beth_xfer));
    xf->buf = buf;
    xf->f_source = send;
    xf->f_done = 0;
    xfer_init(&xf->xf, remote->peer, remote->offband, local->offband, buf->data,
              0, buf->size, send ? xft_push : xft_receive);
    xfer_queue(&xf->xf);
    return xf;
}

uint8 cn_transfer_finished(cn_xfer tr)
{
    return xfer_finished(&tr->xf);
}

void cn_transfer_del(cn_xfer xf)
{
    if(!cn_transfer_finished(xf))
        dbglog("beth: WARN: delete unfinished cn_transfer\n");
    cn_free(xf);
}

void cn_handle_store(cn_handle_p hand, void *data)
{
    uint32 *d = data;
    d[0] = hand->peer.chip;
    d[1] = hand->peer.mod;
    d[2] = (uintptr)hand->offband;
}

void cn_handle_load(cn_handle_p hand, void *data)
{
    uint32 *d = data;
    hand->peer.chip = d[0];
    hand->peer.mod = d[1];
    hand->offband = (void *)d[2];
}

void cn_handle_log(cn_handle_p hand)
{
    cnlog("%d.%d.%#x", hand->peer.chip, hand->peer.mod, hand->offband);
}

cn_handle_p _beth_gethandle(cn_handle_p free);

cn_handle_p cn_handle_alloc()
{
    return _beth_gethandle(0);
}

void cn_handle_free(cn_handle_p hand)
{
    _beth_gethandle(hand);
}

void cn_wait()
{
    os_wait();
}

void _beth_loader_update();

void beth_update()
{
    xfer_update();
    dq_update();
    _beth_loader_update();
}

//TODO don't make this global because only the cm needs this
struct beth_loader_t {
    uint64              offband;
    cn_handle_t         handle;
    uint8               occupied;
    cn_handle_p         peer;
    strbuf              key[CN_BINARY_KEYLEN];
    uint32              param;
};

struct beth_loader_t * beth_loader = 0;

struct beth_loader_state_t {
    void *              buffer;
    uint32              buf_pos;
    uint32              buf_amount;
    uint32              bin_pos;
    uint32              bin_size;
    uint32              block_pos;
    uint32              block_len;
    void *              block_dest;
    void *              grub_addr;
    uint32              grub_data;
    struct beth_loader_t *curr;
    uint32              curr_idx;
    xfer                xf;
    uint8               f_started : 1;
    uint8               f_grub : 1;
};

struct beth_loader_state_t *beth_loader_state;

void beth_init()
{
    uint32 modid = grub_get(0);
    beth_loader_state = mem_malloc(sizeof(struct beth_loader_state_t));
    beth_loader_state->buffer = mem_malloc(BETH_LOADER_BLOCK);
    beth_loader_state->curr = 0;
    beth_loader_state->bin_pos = 0; //last index used
    beth_loader = mem_malloc(sizeof(struct beth_loader_t) * BETH_LOADER_SLOTS);
    uint32 i;
    for(i = 0; i < BETH_LOADER_SLOTS; i++){
        beth_loader[i].handle.peer = noc_makeadr(0, modid);
        beth_loader[i].handle.offband = mod_int2ext(&beth_loader[i].offband);
        beth_loader[i].offband = 0;
        beth_loader[i].occupied = 0;
    }
}

void _beth_loader_update()
{
    struct beth_loader_state_t *s = beth_loader_state;
    if(s == 0)
        return;
    if(s->curr == 0){
        uint32 i, n;
        for(i = 0; i < BETH_LOADER_SLOTS; i++){
            n = (i + s->bin_pos) % BETH_LOADER_SLOTS;
            if(beth_loader[n].occupied && beth_loader[n].offband != 0)
                break;
        }
        if(i == BETH_LOADER_SLOTS)
            return; //nothing to load
        s->curr = beth_loader + n;
        dbglog("beth: loader loading #%d\n", n);
        s->buf_pos = 0;
        s->buf_amount = 0;
        s->bin_pos = 0;
        s->bin_size = 8;
        s->block_pos = 0;
        s->block_len = 0;
        s->block_dest = 0;
        s->curr_idx = n;
        s->f_started = 0;
        s->f_grub = 0;
        s->grub_data = beth_loader[n].offband & 0xffffffff;
        s->grub_addr = (void *)(uintptr)(beth_loader[n].offband >> 32);
        dbglog("beth: loader grub:%#x @%#x\n", s->grub_data, s->grub_addr);
        //stop pe
        //dbglog("beth: loader stop peer;\n");
        *(uint64 *)s->buffer = 0x2;
        xfer_init(&s->xf, s->curr->peer->peer, 0, 0, s->buffer,
            (void *)0x00030008, 8, xft_send); 
        xfer_queue(&s->xf);
        dbgexec(0, 0, s->curr->peer->peer);
    }
    if(!xfer_finished(&s->xf))
        return;
    ////finish
    //disconnect
    if(s->f_started){
        dbglog("beth: loader finished;\n");
        s->curr->occupied = 0;
        s->curr = 0;
        return;
    }
    //send grub
    if(s->f_grub){
        dbglog("beth: loader start peer;\n");
        *(uint64 *)s->buffer = 0x3;
        xfer_init(&s->xf, s->curr->peer->peer, 0, 0, s->buffer,
            (void *)0x00030008, 8, xft_send); 
        s->f_started = 1;
        xfer_queue(&s->xf);
        return;
    }
    //start PE
    if(s->bin_pos >= s->bin_size && s->buf_pos >= s->buf_amount){
        dbglog("beth: loader write grub %#x @ %#x\n", s->grub_data, s->grub_addr);
        *(uint64 *)s->buffer = s->grub_data;
        xfer_init(&s->xf, s->curr->peer->peer, 0, 0, s->buffer,
            s->grub_addr, 8, xft_send); 
        s->f_grub = 1;
        xfer_queue(&s->xf);
        return;
    }
    //// data crunshing
    //load new data
    if(s->buf_pos >= s->buf_amount){
        uint32 bin_size = s->bin_size == 8 ? BETH_LOADER_BLOCK : s->bin_size;
        uint32 amount = MIN(bin_size - s->bin_pos, BETH_LOADER_BLOCK);
        //dbglog("beth: loader get block pos:%#x size:%#x src:%#x\n", s->bin_pos,
        //    amount, s->curr->bin->pos);
        xfer_init(&s->xf, noc_makeadr(0, thm_ddr), 0, 0, s->buffer,
            0, amount, xft_fetch); //FIXME
        s->bin_pos += amount;
        s->buf_pos = 0;
        s->buf_amount = amount;
        xfer_queue(&s->xf);
        return;
    }
    //read bin size header
    if(s->bin_size == 8){
        s->bin_size = *(uint64 *)s->buffer;
        s->buf_pos = 8;
        //dbglog("beth: loader bin size:%d|%#x\n", s->bin_size, s->bin_size);
    }
    //read block header
    if(s->block_pos >= s->block_len){
        if(s->buf_pos + 8 >= s->buf_amount)
            cnerr("beth: bin format broken\n");
        uint32 *head = s->buffer + s->buf_pos;
        s->block_len = head[0];
        s->block_dest = (void *)head[1];
        s->block_pos = 0;
        s->buf_pos += 8;
        //dbglog("beth: loader read block header len:%#x dest:%#x\n", s->block_len,
        //    s->block_dest);
    }
    //write block to module
    uint32 amount = MIN(s->block_len - s->block_pos, s->buf_amount - s->buf_pos);
    xfer_init(&s->xf, s->curr->peer->peer, 0, 0, s->buffer + s->buf_pos,
        s->block_dest + s->block_pos, amount, xft_send);
    //dbglog("beth: loader send block (part) dest:%#x size:%#x\n", s->block_dest +
    //    s->block_pos, amount);
    s->block_pos += amount;
    s->buf_pos += amount;
    xfer_queue(&s->xf);
}

cn_handle_p _beth_loader_alloc(cstr key, cn_handle_p peer)
{
    if(beth_loader == 0)
        dbgerr("beth: loader not inited\n");
    uint32 i;
    for(i = 0; i < BETH_LOADER_SLOTS; i++){
        if(beth_loader[i].occupied == 0){
            dbglog("beth: loader new load request; handle:");
            cn_handle_log(peer);
            dbglog(" #%d\n", i);
            beth_loader[i].occupied = 1;
            beth_loader[i].peer = peer;
            return &beth_loader[i].handle;
        }
    }
    cnerr("beth: cannt load another binary - no resources\n");
    return 0;
}

extern uint8 _end;

void beth_setgrub(void *addr, uint32 data)
{
    if((void *)&_end > addr)
        cnlog("beth: WARNING grub address is not in dynamic memory\n");
    beth_grub_addr = mod_int2ext(addr);
    beth_grub_data = data;
}

struct beth_handle_t    beth_hand[BETH_HANDLESLOTS];
uint64                  beth_offband[BETH_HANDLESLOTS];
uint8                   beth_used[BETH_HANDLESLOTS] = {0}; 
th_adr                  beth_adr = {0, 0};

cn_handle_p _beth_gethandle(cn_handle_p free)
{
    if(beth_adr.mod == 0)
        beth_adr.mod = grub_get(0);
    if(free){
        uint32 i;
        for(i = 0; i < BETH_HANDLESLOTS; i++){
            if(free == beth_hand + i){
                beth_used[i] = 0;
                return 0;
            }
        }
        dbgerr("rte : cannot free handle - not found\n");
    }
    uint32 i;
    for(i = 0; i < BETH_HANDLESLOTS; i++){
        if(beth_used[i] == 0)
            break;
    }
    if(i >= BETH_HANDLESLOTS)
        dbgerr("we ran out of handles - BETH_HANDLESLOTS\n");
    //prepare
    beth_hand[i].peer = beth_adr;
    beth_hand[i].offband = mod_int2ext(beth_offband + i);
    beth_used[i] = 1;
    return beth_hand + i;
}

void cn_memset(void *dst, uint8 fill, uint32 size)
{
    memset(dst, fill, size);
}

void cn_memcpy(void *dst, void *src, uint32 size)
{
    memcpy(dst, src, size);
}

void cn_binary_exec(cstr key, uint32 param)
{
    cnerr("not implemented\n");
}
