#include <m3/VPE.h>
#include <m3/com/RecvGate.h>
#include "canoe.h"

struct m3be_bin_t {
    cstr            key;
    cn_call         call;
};

void m3be_vpenr_set(uint32 x);
void m3be_setbintab(m3be_bin_t *tab);
void m3be_fifomaster(m3::VPE &vpe, cn_io *upp);
void m3be_fifomaster2(m3::VPE &vpe, cn_io up, cn_io *dnp);
void m3be_fifoslave(cn_io *dnp, cn_io *upp);
m3::RecvGate m3be_slave_recv_gate(m3::VPE &vpe);
void m3be_name_set(cstr newname);
void m3be_rgate_init();


extern uint32 m3be_smuggle[10];
