#include "basetypes.h"

#define CN_HANDLE_STRUCTSIZE    sizeof(struct m3be_handle_t)
#define CN_HANDLE_STORESIZE     20

struct m3be_handle_t {
    uint64          internal;
    uint32          vpe;        //manager internal numbering
    uint32          native_cap; //message cap created by the vpe
    uint32          alien_cap;  //space for the peers message cap
};

struct m3be_buf_t {
    uint8 *         data;
    uint32          size;
};

struct m3be_xfer_t;

struct m3be_io_t;

typedef struct m3be_handle_t *  cn_handle_p;
typedef struct m3be_handle_t    cn_handle_t;
typedef struct m3be_buf_t *     cn_buf;
typedef struct m3be_io_t *      cn_io;
typedef struct m3be_xfer_t *    cn_xfer;
