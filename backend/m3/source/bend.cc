#include "config_canoe.h"
#define CANOE_LOGLEVEL CN_M3BE_LOGLEVEL
#include "canoe.h"
#include "m3be.h"
#include <m3/com/RecvGate.h>
#include <m3/com/SendGate.h>
#include <m3/VPE.h>
#include <m3/com/GateStream.h>
#include <m3/Syscalls.h>
#include <m3/stream/Standard.h>
#include <base/KIF.h>

#define M3BE_COMM_BUF   2048
#define M3BE_COMM_MSG   128
#define M3BE_XFER_MSG   128
#define M3BE_COMM_CRED  2048
#define M3BE_WAIT_TICKTHRESH    500

uint32          m3be_vpenr = 0;
uint32          m3be_smuggle[10];
m3be_bin_t *    m3be_bintab;
cn_xfer         m3be_xfer_first = 0;
cn_io           m3be_fifo_first = 0;
uint32          m3be_waitcount = 0;
uint32          m3be_waitloop = 0;

void m3be_vpenr_set(uint32 x)
{
    m3be_vpenr = x;
}

inline uint32 min(uint32 x, uint32 y)
{
    return x < y ? x : y;
}

struct m3be_msg_t {
    uint8               data[M3BE_COMM_MSG];
    uint32              len;
    m3be_msg_t *        next;
};

void m3be_recv_handler(m3::GateIStream &is);
void m3be_reply_handler(m3::GateIStream &is);

struct m3be_rgate_t {
    m3be_rgate_t() :
        gate(m3::RecvGate::create(  m3::nextlog2<M3BE_COMM_BUF>::val,
                                    m3::nextlog2<M3BE_COMM_MSG>::val)),
        reply(m3::RecvGate::create( m3::nextlog2<512>::val,
                                    m3::nextlog2<32>::val))
        {
            start();
        };
    void start(){
        gate.start(m3be_recv_handler);
        reply.start(m3be_reply_handler);
    }
    static void init(){
        if(inst)
            delete inst;
        inst = new m3be_rgate_t();
    };
    static m3be_rgate_t *inst;
    m3::RecvGate gate;
    m3::RecvGate reply;
};

m3be_rgate_t * m3be_rgate_t::inst = 0;

struct m3be_io_recv_t {
    m3be_io_recv_t(uint8 ownrgate) :
        gate(m3::RecvGate::create(m3::nextlog2<M3BE_COMM_BUF>::val, m3::nextlog2<M3BE_COMM_MSG>::val)),
        sendgate(m3::SendGate::create(ownrgate ? &gate : &m3be_rgate_t::inst->gate, (label_t)this, M3BE_COMM_CRED)),
        msg(0), msg_tail(0),
        msg_pos(0), f_ownrgate(ownrgate)
    {
        cnlog4("create recv @%#p\n", this);
    };
    void msg_fin(){
        msg = msg->next;
        if(msg == 0)
            msg_tail = 0;
        msg_pos = 0;
    }
    void start(){
        if(f_ownrgate){
            cnlog("m3be: start rgate sel:%d\n", gate.sel());
            gate.start(m3be_recv_handler);
        }
    }
    m3::RecvGate            gate;
    m3::SendGate            sendgate;
    m3be_msg_t *            msg;
    m3be_msg_t *            msg_tail;
    uint32                  msg_pos;
    uint8                   f_ownrgate;
};

struct m3be_io_send_t {
    m3be_io_send_t(capsel_t cap) :
        gate(m3::SendGate::bind(cap, &m3be_rgate_t::inst->reply,
            m3::ObjCap::KEEP_CAP | m3::ObjCap::KEEP_SEL, (label_t)this)),
        flying(0)
    {};
    void sent() {
        flying += 1;
    }
    m3::SendGate    gate;
    uint32          flying;
};

struct m3be_io_t {
    m3be_io_recv_t *    recv;
    m3be_io_send_t *    send;
    cn_buf              buf;
    uint32              rd;
    uint32              wr;
    uint32              lock;
    m3be_io_t *         next;
};

struct m3be_xfer_t {
    struct m3be_buf_t * buf;
    m3be_io_recv_t *    recv;
    m3be_io_send_t *    send;
    uint32              pos;
    uint8               finished;
    uint8               f_started;
    m3be_xfer_t *       next;
};


void m3be_xfer_update(cn_xfer xf);

void m3be_recv_handler(m3::GateIStream &is){
    m3be_io_recv_t *recv = is.label<m3be_io_recv_t *>();
    m3be_msg_t *newmsg = new m3be_msg_t;
    cn_memcpy(newmsg->data, is.buffer(), is.length());
    newmsg->len = is.length();
    newmsg->next = 0;
    if(!recv->msg)
        recv->msg = newmsg;
    else
        recv->msg_tail->next = newmsg;
    recv->msg_tail = newmsg;
    //cnlog("m3be: got message for recv@:%#x len:%d\n", recv, is.length());
    is.reply(recv, 4);
};

//recv credits
void m3be_reply_handler(m3::GateIStream &is){
    m3be_io_send_t *send = is.label<m3be_io_send_t *>();
    send->flying -= 1;
    //cnlog("m3be: got reply for:%llx\n", is.label<uint64>());
};

void m3be_rgate_init()
{
    m3be_rgate_t::init();
}

extern "C" {

void * cn_malloc(uint32 size)
{
    return m3::Heap::alloc(size);
}

void cn_free(void *p)
{
    m3::Heap::free(p);
}

cn_handle_p cn_handle_alloc()
{
    cn_handle_p hand = (cn_handle_p)cn_malloc(sizeof(cn_handle_t));
    m3be_io_recv_t *recv = new m3be_io_recv_t(1);
    hand->vpe = m3be_vpenr;
    hand->native_cap = recv->sendgate.sel();
    hand->alien_cap = m3::VPE::self().alloc_cap();
    hand->internal = (uintptr)recv;
    return hand;
}

void cn_handle_free(cn_handle_p hand)
{
    m3be_io_recv_t *recv = (m3be_io_recv_t *)hand->internal;
    if(recv)
        delete recv;
    cn_free(hand);
}

void cn_handle_log(cn_handle_p hand)
{
    cnlog("[vpe:%d native:%d alien:%d ud:%#llx]", hand->vpe, hand->native_cap,
        hand->alien_cap, hand->internal);
}

cn_io cn_fifo(uint32 size, uint32 wrap, cn_handle_p local, cn_handle_p remote, uint8 sink)
{
    //cnlog("cn_fifo: malloc\n");
    cn_io io = (cn_io)cn_malloc(sizeof(m3be_io_t));
    cnlog3("m3be: created fifo @:%#p sink:%d\n", io, sink);
    cn_memset(io, 0, sizeof(m3be_io_t));
    //cnlog("cn_fifo: buffer\n");
    io->buf = cn_buffer(wrap + 5, 0);
    if(sink){
        //cnlog("cn_fifo: send struct\n");
        io->send = new m3be_io_send_t(local->alien_cap);
        io->recv = 0;
    }else{
        //cnlog("cn_fifo: recv struct\n");
        io->recv = (m3be_io_recv_t *)local->internal;
        io->recv->start();
        //cnlog("cn_fifo: /recv start\n");
        local->internal = 0; //prevent destruction of our recvgate
        io->send = 0;
    }
    //cnlog("cn_fifo: done\n");
    //linked list
    io->next = m3be_fifo_first;
    m3be_fifo_first = io;
    return io;
}

cn_buf cn_buffer(uint32 size, void *data)
{
    if(data != 0)
        cnerr("cannot use data parameter yet\n");
    cn_buf buf = (cn_buf)cn_malloc(sizeof(cn_buf) + size);
    buf->data = (uint8 *)(buf + 1);
    buf->size = size;
    return buf;
}

void cn_buffer_del(cn_buf buf)
{
    cn_free(buf);
}

cn_io cn_access(cn_buf buf, uint8 filled)
{
    cn_io io = (cn_io)cn_malloc(sizeof(m3be_io_t));
    cn_memset(io, 0, sizeof(m3be_io_t));
    io->buf = buf;
    if(filled)
        io->wr = buf->size;
    return io;
}

void cn_io_del(cn_io io)
{
    if(io->recv)
        delete io->recv;
    if(io->send)
        delete io->send;
    cn_free(io->buf);
    //dequeue
    if(m3be_fifo_first == io)
        m3be_fifo_first = io->next;
    else{
        cn_io itm = m3be_fifo_first;
        while(1){
            if(itm->next == 0)
                cnerr("cannot find fifo to dequeue\n");
            if(itm->next == io){
                itm->next = io->next;
                break;
            }
            itm = itm->next;
        }
    }
    cn_free(io);
}

void * cn_poke(cn_io io, uint32 size)
{
    if(size > io->buf->size)
        cnerr("peek for block bigger buffer\n");
    if(io->wr + size > io->buf->size)
        return 0;
    io->lock = size;
    return (void *)(io->buf->data + io->wr);
}

void cn_push(cn_io io)
{
    io->wr += io->lock;
    io->lock = 0;
}

void * cn_peek(cn_io io, uint32 size)
{
    if(io->recv){
        //cnlog("<DBG> peek on fifo size:%d\n", size);
        if(io->wr < size){
            //get new message
            if(io->recv->msg == 0)
                return 0;
            uint32 msglen = io->recv->msg->len;
            uint32 avail = msglen - io->recv->msg_pos;
            uint32 need = size - io->wr;
            uint32 move = min(avail, need);
            cn_memcpy(io->buf->data + io->wr,
                io->recv->msg->data + io->recv->msg_pos, move);
            io->recv->msg_pos += move;
            io->wr += move;
            if(io->recv->msg_pos == msglen)
                io->recv->msg_fin();
            if(io->wr < size)
                return 0;
        }
    }else{
        if(io->rd + size > io->wr)
            return 0;
    }
    io->lock = size;
    //cnlog("<DBG> peeked %d bytes\n", size);
    return (void *)(io->buf->data + io->rd);  
}

void cn_pop(cn_io io)
{
    io->rd += io->lock;
    io->lock = 0;
    if(io->recv){
        if(io->rd < io->wr)
            cn_memcpy(io->buf->data, io->buf->data + io->rd, io->wr - io->rd);
        io->wr -= io->rd;
        io->rd = 0;
    }
}

void cn_handle_prep(cn_handle_p h1, cn_handle_p h2)
{
    cnlog3("m3be: prepare"); cn_handle_log(h1); cnlog(" and ");
    cn_handle_log(h2); cnlog("\n");
    capsel_t tmp = m3::VPE::self().alloc_caps(2);
    m3::KIF::CapRngDesc tmp1(m3::KIF::CapRngDesc::OBJ, tmp, 1);
    m3::KIF::CapRngDesc tmp2(m3::KIF::CapRngDesc::OBJ, tmp + 1, 1);
    m3::Syscalls::get().exchange(h1->vpe, tmp1, h1->native_cap, 1);
    m3::Syscalls::get().exchange(h2->vpe, tmp1, h2->alien_cap, 0);
    m3::Syscalls::get().exchange(h2->vpe, tmp2, h2->native_cap, 1);
    m3::Syscalls::get().exchange(h1->vpe, tmp2, h1->alien_cap, 0);
}   

uint32 cn_handle_store(cn_handle_p hand, void *data)
{
    uint32 *d = (uint32 *)data;
    *(uint64 *)data = hand->internal;
    d[2] = hand->vpe;
    d[3] = hand->native_cap;
    d[4] = hand->alien_cap;
    return CN_HANDLE_STORESIZE;
}

uint32 cn_handle_load(cn_handle_p hand, void *data)
{
    uint32 *d = (uint32 *)data;
    hand->internal = *(uint64 *)data;
    hand->vpe = d[2];
    hand->native_cap = d[3];
    hand->alien_cap = d[4];
    return CN_HANDLE_STORESIZE;
}

cn_xfer cn_transfer(cn_buf buf, cn_handle_p local, cn_handle_p remote, uint8 send)
{
    cnlog("m3be: create xfer!\n");
    cn_xfer xf = (cn_xfer)cn_malloc(sizeof(m3be_xfer_t));
    cn_memset(xf, 0, sizeof(m3be_xfer_t));
    xf->buf = buf;
    if(send){
        xf->send = new m3be_io_send_t(local->alien_cap);
        xf->recv = 0;
    }else{
        xf->recv = (m3be_io_recv_t *)local->internal;
        xf->recv->start();
        local->internal = 0;
        xf->send = 0;
    }
    //add to list
    xf->next = m3be_xfer_first;
    m3be_xfer_first = xf;
    m3be_xfer_update(xf);
    return xf;
}

uint8 cn_transfer_finished(cn_xfer xf)
{
    if(xf->send && xf->send->flying != 0)
        return false;
    return xf->finished;
}

void cn_transfer_start(cn_xfer xf)
{
    xf->f_started = 1;
}

void cn_transfer_del(cn_xfer xf)
{
    if(!cn_transfer_finished(xf))
        cnlog("WARN: deleting not finished transfer\n");
    if(xf->recv)
        delete xf->recv;
    if(xf->send)
        delete xf->send;
    //unqueue
    if(m3be_xfer_first == xf)
        m3be_xfer_first = xf->next;
    else{
        cn_xfer itm = m3be_xfer_first;
        while(1){
            if(itm->next == 0)
                cnerr("cannot find transfer to dequeue\n");
            if(itm->next == xf){
                itm->next = xf->next;
                break;
            }
            itm = itm->next;
        }
    }
    cn_free(xf);
    cnlog("m3be: freed xfer @:%#p\n", xf);
}

} //extern "C"

void m3be_xfer_update(cn_xfer xf)
{
    if(!xf->f_started)
        return;
    //send
    if(xf->send){
        uint32 avail = xf->buf->size - xf->pos;
        uint32 block = min(M3BE_XFER_MSG - 30, avail);
        if(block == 0)
            return;
        cnlog("m3be: send xfer msg size:%d/%d\n", block, avail);
        uint32 *pack = (uint32 *)(xf->buf->data + xf->pos);
        if(m3::send_msg(xf->send->gate, pack, block) != m3::Errors::NONE)
            return;
        xf->send->sent();
        xf->pos += block;
    }else{ //receive
        if(xf->recv->msg == 0)
            return;
        uint32 cp = min(xf->recv->msg->len, xf->buf->size - xf->pos);
        cn_memcpy(xf->buf->data + xf->pos, xf->recv->msg->data, cp);
        xf->pos += cp;
        xf->recv->msg_fin();
        cnlog("m3be: xfer recvd size:%d\n", cp);
    }
    if(xf->pos == xf->buf->size){
        xf->finished = 1;
        cnlog("m3be: xfer is now finished\n");
    }
}

void m3be_fifo_update(cn_io ff)
{
    if(ff->send && ff->rd < ff->wr){
        uint32 block = min(ff->wr - ff->rd, M3BE_COMM_MSG - 30);//FIXME hardcoded header size
        uint32 *pack = (uint32 *)(ff->buf->data + ff->rd);
        cnlog3("m3be: send on fifo @:%#p size:%d/%d gate:@%#p sel:%d\n",
            ff, block, ff->wr - ff->rd, &ff->send->gate, ff->send->gate.sel());
        if(m3::send_msg(ff->send->gate, pack, block) != m3::Errors::NONE){
            //cnlog("m3be: send blocked @:%#x\n", ff);
            return;
        }
        ff->send->sent();
        //cnlog("m3be: ? %d\n", ff->send->gate.sel());
        //m3::receive_reply(ff->send->gate); //TODO: make async
        //cnlog("m3be: !\n");
        ff->rd += block;
    }
    if(ff->send && ff->rd == ff->wr && ff->lock == 0){
        ff->rd = 0;
        ff->wr = 0;
    }
}

void m3be_fifomaster(m3::VPE &vpe, cn_io *upp)
{
    //up fifo
    cn_io up = (cn_io)cn_malloc(sizeof(m3be_io_t));
    cn_memset(up, 0, sizeof(m3be_io_t));
    up->buf = cn_buffer(M3BE_COMM_MSG, 0);
    up->recv = new m3be_io_recv_t(0);
    //queue
    up->next = m3be_fifo_first;
    m3be_fifo_first = up;
    capsel_t upcap = up->recv->sendgate.sel();
    vpe.delegate_obj(upcap);
    m3be_smuggle[0] = upcap;
    *upp = up;
}

void m3be_fifomaster2(m3::VPE &vpe, cn_io up, cn_io *dnp)
{
    uint32 *d = 0;
    cnlog("m3be: fetch cap from worker...\n");
    while(0 == (d = (uint32 *)cn_peek(up, 4)))
        cn_wait();
    cnlog("m3be: got cap from worker sel:%d\n", d[0]);
    capsel_t dncap_o = d[0];
    cn_pop(up);
    capsel_t dncap_s = m3::VPE::self().alloc_cap();
    m3::KIF::CapRngDesc crg(m3::KIF::CapRngDesc::OBJ, dncap_s, 1);
    if(m3::Errors::Code::NONE != m3::Syscalls::get().exchange(vpe.sel(), crg, dncap_o, 1))
        cnerr("cannot exchange cap\n");
    //down fifo
    cn_io dn = (cn_io)cn_malloc(sizeof(m3be_io_t));
    cn_memset(dn, 0, sizeof(m3be_io_t));
    dn->buf = cn_buffer(M3BE_COMM_MSG, 0);
    dn->send = new m3be_io_send_t(dncap_s);
    dn->next = m3be_fifo_first;
    m3be_fifo_first = dn;
    *dnp = dn;
}

void m3be_fifoslave(cn_io *dnp, cn_io *upp)
{
    //up fifo
    cn_io up = (cn_io)cn_malloc(sizeof(m3be_io_t));
    cnlog3("m3be: created fifo (su) @:%#p\n", up);
    cn_memset(up, 0, sizeof(m3be_io_t));
    up->buf = cn_buffer(M3BE_COMM_MSG, 0);
    up->send = new m3be_io_send_t(m3be_smuggle[0]);
    up->next = m3be_fifo_first;
    m3be_fifo_first = up;
    *upp = up;
    //down fifo
    cn_io dn = (cn_io)cn_malloc(sizeof(m3be_io_t));
    cnlog3("m3be: created fifo (sd) @:%#p\n", dn);
    cn_memset(dn, 0, sizeof(m3be_io_t));
    dn->buf = cn_buffer(M3BE_COMM_MSG, 0);
    dn->recv = new m3be_io_recv_t(0);
    dn->next = m3be_fifo_first;
    m3be_fifo_first = dn;
    *dnp = dn;
    //send cap
    uint32 *d = (uint32 *)cn_poke(up, 4);
    if(d == 0)
        cnerr("no way!!!\n");
    d[0] = dn->recv->sendgate.sel();
    cn_push(up);
}
   

m3::RecvGate m3be_slave_recv_gate(m3::VPE &vpe)
{
    return m3::RecvGate::create_for(vpe, m3::getnextlog2(M3BE_COMM_BUF),
        m3::getnextlog2(M3BE_COMM_MSG));
}

void cn_memset(void *dst, uint8 fill, uint32 size)
{
    memset(dst, fill, size);
}

void cn_memcpy(void *dst, const void *src, uint32 size)
{
    memcpy(dst, src, size);
}

void cn_wait(){
    m3be_waitcount++;
    if(m3be_waitcount > M3BE_WAIT_TICKTHRESH){
        m3be_waitcount -= M3BE_WAIT_TICKTHRESH;
        m3be_waitloop++;
        cnlog("m3be: wait #%d\n", m3be_waitloop);
    }
    m3::env()->workloop()->tick();
    for(cn_xfer xf = m3be_xfer_first; xf != 0; xf = xf->next)
        m3be_xfer_update(xf);
    for(cn_io xf = m3be_fifo_first; xf != 0; xf = xf->next)
        m3be_fifo_update(xf);
}

void cn_binary_exec(cstr key, uint32 param)
{
    cnlog2("m3be: loading binary key:%s\n", key);
    if(!m3be_bintab)
        cnerr("no bintab loaded\n");
    for(uint32 i = 0; m3be_bintab[i].key; i++){
        if(!strcmp(key, m3be_bintab[i].key)){
            cn_kern_setkernel(m3be_bintab[i].call);
            return;
        }
    }
    cnerr("cannot load binary - not found key:%s\n", key);
}

void m3be_setbintab(m3be_bin_t *tab)
{
    m3be_bintab = tab;
}
