#include "config_canoe.h"
#define CANOE_LOGLEVEL CN_M3BE_LOGLEVEL
#include "canoe.h"
#include "m3be.h"
#include <m3/stream/Standard.h>

struct m3be_valist {
    va_list l;
};

uint8           m3be_printf_nl = 1;     //newline marker
cstr            m3be_name = "non";
extern cn_xfer  m3be_xfer_first;

void m3be_name_set(cstr newname)
{
    m3be_name = newname;
}

uint32 _m3be_printf_lit(const char *fmt, uint32 start)
{
    if(m3be_printf_nl){
        m3be_printf_nl = 0;
        m3::cout << m3be_name << " ";
    }
    uint32 i;
    for(i = start; i < start + 50; i++){
        if(fmt[i] == '%' || fmt[i] == 0 || fmt[i] == '\n')
            break; 
    }
    if(i > start){
        char buf[50];
        memcpy(buf, fmt + start, i - start);
        buf[i - start] = 0;
        //m3::cout << "#LIT#(" << buf << ")";
        m3::cout << buf;
    }
    if(fmt[i] == '\n'){
        m3::cout << "\n";
        m3be_printf_nl = 1;
        i++;
    }
    return i;
}

uint32 _m3be_printf_abort(const char *fmt, uint32 pos, const char *msg)
{
    m3::cout << " #ERR#(" << (fmt + pos) << ") #RES#(" << msg << ")\n";
    uint32 i;
    for(i = pos; fmt[i] != 0; i++);
    return i;
}

uint32 _m3be_printf_param(const char *fmt, uint32 start, m3be_valist *lst)
{
    if(fmt[start] != '%')
        return start;
    if(fmt[start + 1] == '%'){
        m3::cout << "%";
        return start + 1;
    }
    //scan characters
    uint32 i;
    uint32 main = 0;    //base type [invalid, str, uint32, uint64, float, double, ptr]
    uint32 ishex = 0;
    uint32 ishash = 0;
    uint32 islong = 0; //1:long 2:longlong
    uint32 issigned = 1;
    for(i = start + 1; fmt[i] != 0; i++){
        switch(fmt[i]){
        case 'd': main = 2; break;
        case 'x': main = 2; ishex = 1; break;
        case 's': main = 1; break;
        case 'p': main = 6; break;
        case '#': ishash = 1; ishex = 1; break;
        case 'l': islong += 1; break;
        case 'u': main = 2; issigned = 0; break;
        default: return _m3be_printf_abort(fmt, start, "bad formatter");
        }
        if(main) break;
    }
    i++;
    //debug
    //m3::cout << "#" << main << "," << issigned << "," << islong << "#";
    //process options
    if(main == 0)
        return _m3be_printf_abort(fmt, start, "bad base type");
    /*if(main == 6){
        if(sizeof(uintptr) == sizeof(uint32)){
            m3::cout << "#P4#";
            main = 2;
        }else if(sizeof(uintptr) == sizeof(uint64)){
            m3::cout << "#P8#";
            main = 3;
        }else
            _m3be_printf_abort(fmt, start, "bad pointer size");
    }*/
    if(main == 2 && islong == 2)
        main = 3;
    //build format string
    strbuf f[10];
    uint32 fp = 0;
    if(ishash)
        f[fp++] = '#'; 
    if(ishex)
        f[fp++] = 'x';
    if(main == 2 || main == 3)
        f[fp++] = 'd';
    f[fp] = 0;
    //print
    if(main == 2){
        uint32 val = va_arg(lst->l, uint32);
        if(issigned)
            m3::cout << m3::fmt((sint32)val, f);
        else
            m3::cout << m3::fmt(val, f);
        return i;
    }
    if(main == 3){
        uint64 val = va_arg(lst->l, uint64);
        if(issigned)
            m3::cout << m3::fmt((sint64)val, f);
        else
            m3::cout << m3::fmt(val, f);
        return i;
    }
    if(main == 6){
        void *val = va_arg(lst->l, void *);
        m3::cout << m3::fmt(val, f);
        return i;
    }
    if(main == 1){
        m3::cout << va_arg(lst->l, char *);
        return i;
    }
    return _m3be_printf_abort(fmt, start, "bad parameter factor constalation");
}

void _m3be_printf(const char *fmt, m3be_valist *lst)
{
    uint32 pos = 0;
    while(fmt[pos] != 0){
        pos = _m3be_printf_lit(fmt, pos);
        pos = _m3be_printf_param(fmt, pos, lst);
    }
}

extern "C" {

void cnerr(const char *fmt, ...)
{
    m3be_valist lst;
    va_start(lst.l, fmt);
    _m3be_printf(fmt, &lst);
    va_end(lst.l);
    ::exit(1);
}

void cnlog(const char *fmt, ...)
{
    m3be_valist lst;
    va_start(lst.l, fmt);
    _m3be_printf(fmt, &lst);
    va_end(lst.l);
}

}