#include "config_cnos.h"
#define LOGLEVEL CNOS_APP_LOGLEVEL
#include "thlib.h"
#include "canoeversion.h"
#include "cn_app.h"
#include "cn_nod.h"

#ifndef CNOS_APP_DN_SIZE
#define CNOS_APP_DN_SIZE    128
#endif
#ifndef CNOS_APP_DN_WRAP
#define CNOS_APP_DN_WRAP    64
#endif
#ifndef CNOS_APP_UP_SIZE
#define CNOS_APP_UP_SIZE    128
#endif
#ifndef CNOS_APP_UP_WRAP
#define CNOS_APP_UP_WRAP    64
#endif
#ifndef CNOS_CM_GRUB
#define CNOS_CM_GRUB        0xb220
#endif
#ifndef CNOS_GRUB_SRC
#define CNOS_GRUB_SRC       13
#endif
#ifndef CNOS_APP_FIFOSLOTS
#define CNOS_APP_FIFOSLOTS  10
#endif

extern void     run(cnnod_man *);
void            cmfifo_init(uint64 *offband, uint64 *offband2, qp *dn, qp *up);
uint32          oswait();
void *          handhook(void *free);

ALIGNCACHE    uint64 offband[C_LSIZE/8];
ALIGNCACHE    uint64 offband2[C_LSIZE/8];

int main(int argc, char *argv[])
{   
    //init the system
    mod_init();                     //exception handlers, etc...
    os_hook(oswait);                //register callback
    dbginit();                      //init logging system
    mem_init();                     //dynamic mem allocation for thlib
    CANOE_LOGVERSION
    THLIB_LOGVERSION
    dbglog1("rte   : log verbosity: %d\n", LOGLEVEL);
    dbglog2("rte   : mem @ %x\n", MEM_MEM_MIN);
    
    //connect to CM
    qp ffdown, ffup;
    cmfifo_init(offband, offband2, &ffdown, &ffup);
    cnnod_man *man = cnnod_man_new(32, ffup, ffdown, handhook);
    dbglog("rte   : init done\n");

    //setup canoe
    dbglog4("rte   : run!\n");
    run(man);

    //finish
    dbglog("rte   : ==== >> burbel burbel - burbel burbel << ====\n");
    while(1){
        cnnod_man_comm(man);
        cnnod_man_work(man);
        cn_wait();
    }
}

//establish connection to the CoreManager
void cmfifo_init(uint64 *offband, uint64 *offband2, qp *dn, qp *up)
{
    dbglog1("rte   : connect to CM...\n");
    grub_set(CNOS_GRUB_SRC, (uintptr)offband);
    grub_set(CNOS_GRUB_SRC+1, (uintptr)offband2);
    //down 
    dbglog2("rte   : connect cm down fifo\n");
    void *r = (void *)grub_getat(CNOS_GRUB_SRC, noc_makeadr(0, thm_cm));
    dqp q1 = dq_create(CNOS_APP_DN_SIZE, CNOS_APP_DN_WRAP);
    dq_setoffband(q1, offband, noc_makeadr(0, thm_cm), r, dqm_sink, 1);
    bethlib_registerdq(q1);
    *dn = q1->q;
    //up
    dbglog2("rte   : connect up down fifo\n");
    r = (void *)grub_getat(CNOS_GRUB_SRC+1, noc_makeadr(0, thm_cm));
    dqp q2 = dq_create(CNOS_APP_UP_SIZE, CNOS_APP_UP_WRAP);
    dq_setoffband(q2, offband2, noc_makeadr(0, thm_cm), r, dqm_source, 1);
    bethlib_registerdq(q2);
    *up = q2->q;
    //wait for connection
    dbglog("rte   : wait for fifos...\n");
    while(dq_connecting(q1) || dq_connecting(q2))
        os_wait();
    dbglog1("rte   : CM found!\n");
    return;
}

//callback - will be called when someone calls os_wait to wait for things to happen 
uint32 oswait()
{
    bethlib_upd();
    xfer_update();
    return 0;
}

ALIGNCACHE uint64   fifo_offband[CNOS_APP_FIFOSLOTS * (C_LSIZE / 8)] = {0};
uint8               fifo_offband_used[CNOS_APP_FIFOSLOTS] = {0}; 

void * handhook(void *free)
{
    if(free){
        uint32 i = ((uintptr)free - (uintptr)fifo_offband) / C_LSIZE;
        fifo_offband_used[i] = 0;
        return 0;
    }
    uint32 i;
    for(i = 0; i < CNOS_APP_FIFOSLOTS; i++){
        if(fifo_offband_used[i] == 0){
            fifo_offband_used[i] = 1;
            uint64 *p = fifo_offband + (i * C_LSIZE / 8);
            *p = 0;
            c_writeback(p);
            c_invalidate(p);
            return p;
        }
    }
    dbgerr("we ran out of fifo slots - CNOS_APP_FIFOSLOTS\n");
    return 0;
}
