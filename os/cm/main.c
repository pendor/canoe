#include "config_cnos.h"
#define LOGLEVEL CNOS_CM_LOGLEVEL
#include "thlib.h"
#include "canoeversion.h"
#include "cn_man.h"
#include "cn_srcdef.h"

#ifndef CNOS_CM_OBJSLOTS
#define CNOS_CM_OBJSLOTS    100
#endif
#ifndef CNOS_CM_NPEERS
#define CNOS_CM_NPEERS      9
#endif
#ifndef CNOS_CM_NPES
#define CNOS_CM_NPES        4
#endif
#ifndef CNOS_PE_GRUB
#define CNOS_PE_GRUB        0xb220
#endif
#ifndef CNOS_CM_GRUB
#define CNOS_CM_GRUB        0xb220
#endif
#ifndef CNOS_CM_APPDN_SIZE
#define CNOS_CM_APPDN_SIZE  80
#endif
#ifndef CNOS_CM_APPDN_WRAP
#define CNOS_CM_APPDN_WRAP  80
#endif
#ifndef CNOS_CM_APPUP_SIZE
#define CNOS_CM_APPUP_SIZE  80
#endif
#ifndef CNOS_CM_APPUP_WRAP
#define CNOS_CM_APPUP_WRAP  80
#endif
#ifndef CNOS_CM_DN_SIZE
#define CNOS_CM_DN_SIZE     80
#endif
#ifndef CNOS_CM_DN_WRAP
#define CNOS_CM_DN_WRAP     80
#endif
#ifndef CNOS_CM_UP_SIZE
#define CNOS_CM_UP_SIZE     80
#endif
#ifndef CNOS_CM_UP_WRAP
#define CNOS_CM_UP_WRAP     80
#endif
#ifndef CNOS_GRUB_SRC
#define CNOS_GRUB_SRC       13
#endif
#ifndef CNOS_GRUB_PE
#define CNOS_GRUB_PE        25
#endif

uint8               cm_running = 1;

void    appfifo_init(cnman_man *man, uint64 *offband, uint64 *offband2);
void    pes_init(cnman_man *man, uint64 *offband);
uint32  oswait();
void    pe_update();
void    app_update();
void    canoework();

int main(int argc, char *argv[])
{
    mod_init();
    mem_init();
    dbginit();
    CANOE_LOGVERSION
    THLIB_LOGVERSION
    dbglog1("rte   : log verbosity: %d\n", LOGLEVEL);
    os_hook(oswait);
    
    dbglog1("rte   : dmem @ %x (0x%x | %d)\n", MEM_DMEM_MIN, MEM_DMEM_SIZE, MEM_DMEM_SIZE);

    cnman_man *man;
    man = cnman_manager_new(CNOS_CM_OBJSLOTS, CNOS_CM_NPEERS);

    uint64 app_fifo_ob, app_fifo2_ob;
    appfifo_init(man, &app_fifo_ob, &app_fifo2_ob);
    
    uint64 pes_offband[CNOS_CM_NPEERS * 2];
    pes_init(man, pes_offband);

    dbglog("rte   : init done\n");

    uint32 loop = 0, counter = 0;

    while(cm_running){
        os_wait();
        cnman_manager_objectwork(man);
        cnman_manager_peerwork(man);
        loop += 1;
        if(loop == 10000){
            loop = 0;
            counter += 1;
            dbglog("rte   : loop %d\n", counter);
        }
    }
    os_endless();
    return 0;
}

void appfifo_init(cnman_man *man, uint64 *offband, uint64 *offband2)
{
    dbglog1("rte   : connect to app...\n");
    dbglog2("rte   : offband1/2: %#x %#x\n", DRAM_I2E(offband), DRAM_I2E(offband2));
    grub_set(CNOS_GRUB_SRC, DRAM_I2E(offband));
    grub_set(CNOS_GRUB_SRC+1, DRAM_I2E(offband2));
    
    //down
    dbglog2("rte   : connect app down fifo\n");
    void *r = (void *)grub_getat(CNOS_GRUB_SRC, noc_makeadr(0, thm_app));
    dqp q1 = dq_create(CNOS_CM_APPDN_SIZE, CNOS_CM_APPDN_WRAP);
    dq_setoffband(q1, offband, noc_makeadr(0, thm_ddr), r, dqm_source, 1);
    bethlib_registerdq(q1);
    //up
    dbglog2("rte   : connect app up fifo\n");
    r = (void *)grub_getat(CNOS_GRUB_SRC+1, noc_makeadr(0, thm_app));
    dqp q2 = dq_create(CNOS_CM_APPUP_SIZE, CNOS_CM_APPUP_WRAP);
    dq_setoffband(q2, offband2, noc_makeadr(0, thm_ddr), r, dqm_sink, 1);
    bethlib_registerdq(q2);
    //
    dbglog2("rte   : wait for fifos...\n");
    while(dq_connecting(q1) || dq_connecting(q2))
        os_wait();
    dbglog1("rte   : found APP\n");
    cnman_peer *peer = cnman_peer_new(noc_makeadr(0, thm_ddr), q2->q, q1->q);
    cnman_manager_addpeer(man, peer);
    return;
}

void pes_init(cnman_man *man, uint64 *offband)
{
    int i;
    dqp ffdn, ffup;
    uint64 *ob;//, recv[2];
    //uint32 snoty;
    th_adr peer;
    cnman_peer *pe;
    dbglog1("rte   : connect PEs\n");
    for(i = 0; i < CNOS_CM_NPES; i++){
        dbglog2("rte   : wait for PE%d...\n", i);
        peer = noc_makepeadr(0, i);
        //xchg
        grub_setat(CNOS_GRUB_PE + 1, DRAM_I2E(offband + 2 * i), peer);
        ob = (uint64 *)grub_getat(CNOS_GRUB_PE, peer);
        //fifo
        ffdn = dq_create(CNOS_CM_DN_SIZE, CNOS_CM_DN_WRAP);
        dq_setoffband(ffdn, offband + 2 * i, peer, ob, dqm_sink, 1);
        bethlib_registerdq(ffdn);
        ffup = dq_create(CNOS_CM_UP_SIZE, CNOS_CM_UP_WRAP);
        dq_setoffband(ffup, offband + 2 * i + 1, peer, ob + 1, dqm_source, 1);
        bethlib_registerdq(ffup);
        //create
        pe = cnman_peer_new(peer, ffdn->q, ffup->q);
        pe->f_worker = 1;
        cnman_manager_addpeer(man, pe);
        //handshake - send pe id
        while(dq_connecting(ffdn) || dq_connecting(ffup))
            os_wait();
        dbglog2("rte   : PE fifos connected\n");
        ptr p;
        p.vs = cn_poke(ffdn->q, 8);
        p.ui64[0] = i;
        cn_push(ffdn->q);
    }
    dbglog1("rte   : connect PEs done\n");
}

uint32 oswait()
{
    bethlib_upd();
    return 0;
}
