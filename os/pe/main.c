#include "config_cnos.h"
#define LOGLEVEL CNOS_PE_LOGLEVEL
#include "thlib.h"
#include "canoeversion.h"
#include "cn_nod.h"

#ifndef CNOS_PE_MANSLOTS
#define CNOS_PE_MANSLOTS    100
#endif
#ifndef CNOS_PE_FIFOSLOTS
#define CNOS_PE_FIFOSLOTS    20
#endif
#ifndef CNOS_PE_GRUB
#define CNOS_PE_GRUB    0xaff8
#endif
#ifndef CNOS_PE_DNSIZE
#define CNOS_PE_DNSIZE      64  
#endif
#ifndef CNOS_PE_DNWRAP
#define CNOS_PE_DNWRAP      64  
#endif
#ifndef CNOS_PE_UPSIZE
#define CNOS_PE_UPSIZE      32  
#endif
#ifndef CNOS_PE_UPWRAP
#define CNOS_PE_UPWRAP      32  
#endif
#ifndef CNOS_GRUB_PE
#define CNOS_GRUB_PE        25
#endif


uint32              peid;
uint32              renotify = 0;

cnnod_man *         man;

void                cmfifos_init(qp *dn, qp *up);
uint32              oswait();
void *              gethandle(void *free);
void                start(cn_id core);
void                work();
void                handle(cnnod_man *man, uint32 idx);

int main(int argc, char *argv[])
{
    mod_init();
    dbginit();
    mem_init();
    CANOE_LOGVERSION
    THLIB_LOGVERSION
    dbglog1("rte   : log verbosity: %d\n", LOGLEVEL);
    dbglog2("rte   : dmem: %x (%x|%d)\n", mem_dmem, mem_size(mem_dmem), mem_size(mem_dmem));
    dbglog2("rte   : imem: %x (%x|%d)\n", mem_imem, mem_size(mem_imem), mem_size(mem_imem));
    if(MEM_DMEM_MIN > PTR(DRAM_E2I(CNOS_PE_GRUB)))
        dbgerr("rte   : GRUB is not in the dynamic memory %x < %x\n", CNOS_PE_GRUB, MEM_DMEM_MIN);
    os_hook(oswait);
    ctxt_init();

    qp up, dn;
    cmfifos_init(&dn, &up);

    man = cnnod_man_new(CNOS_PE_MANSLOTS, dn, up, gethandle);

    //cnnod_sendmem(man->fifo_send, mem_size(mem_dmem), mem_size(mem_imem), 0);

    dbglog("rte   : init done\n");

    uint32 x, loop = 0;
    uint32 count = 0;
    while(1){
        //if(count < 1){
            os_wait();
            x = cnnod_man_comm(man);
            cnnod_man_work(man);
            handle(man, x);
        //}
        loop += 1;
        if(loop >= 100000){
            dbglog2("rte   : loop %d\n", count);
            loop = 0;
            count += 1;
        }
        //work();
    }
    
    dbglog("rte   :  === >> burbel burbel - burbel burbel << ===\n");
    
    dbgexitcode(1);
    
    os_endless();
    
    return 0;
}

void cmfifos_init(qp *dn, qp *up)
{
    ptr loc_ob1;
    ptr loc_ob2;
    ptr rem_ob;
    dbglog1("rte   : connect to CM...\n");
    loc_ob1.vs = gethandle(0);
    loc_ob2.vs = gethandle(0);
    if(loc_ob1.ui64 + 1 != loc_ob2.ui64)
        dbgerr("failt to get consecutive handles\n");
    grub_set(CNOS_GRUB_PE, DRAM_I2E(loc_ob1.ptr));
    rem_ob.ptr = grub_get(CNOS_GRUB_PE + 1);
    dbglog2("rte   : grub done\n");
    dqp q1 = dq_create(CNOS_PE_DNSIZE, CNOS_PE_DNWRAP);
    dq_setoffband(q1, loc_ob1.vs, noc_makeadr(0, thm_cm), rem_ob.vs, dqm_source, 1);
    bethlib_registerdq(q1);
    *dn = q1->q;
    dqp q2 = dq_create(CNOS_PE_UPSIZE, CNOS_PE_UPWRAP);
    rem_ob.ui64 += 1;
    dq_setoffband(q2, loc_ob2.vs, noc_makeadr(0, thm_cm), rem_ob.vs, dqm_sink, 1);
    bethlib_registerdq(q2);
    *up = q2->q;
    dbglog1("rte   : found CM\n");
    while(dq_connecting(q1) || dq_connecting(q2))
        os_wait();
    dbglog2("rte   : fifos connected!\n");
    ptr p;
    p.vs = cn_peek(*dn, 8);
    peid = p.ui64[0];
    cn_pop(*dn);
    dbglog1("rte   : me is PE %d\n", peid);
}

uint32 oswait()
{
    bethlib_upd();
    xfer_update();
    return 0;
}

uint32 lastthread = 0;

void work(){
    sint32 i, first = -1, next = -1;
    for(i = 0; i < man->objs_s; i++){
        if(man->objs[i].typ != not_thread)
            continue;
        if(first == -1)
            first = i;
        if(next == -1 && i > lastthread){
            next = i;
            break;
        }
    }
    if(next != -1)
        lastthread = next;
    else if(first != -1)
        lastthread = first;
    else //no threads there at all
        return;
    //dbglog("switch to # %d\n", lastthread);
    ctxt_switch_return(man->objs[lastthread].thrd);
}

void handle(cnnod_man *man, uint32 x)
{
    if(x == 0xffffffff)
        return;
    if(man->objs[x].typ == not_transfer){
        dbglog2("rte   : run transfer\n");
        while(man->objs[x].typ == not_transfer){
            cnnod_transfer_work(man->objs[x].trans, man);
            os_wait();
        }
    }
}

uint64              fifo_offband[CNOS_PE_FIFOSLOTS];
uint8               fifo_offband_used[CNOS_PE_FIFOSLOTS] = {0}; 

void * gethandle(void *free)
{
    if(free){
        uint32 i = ((uintptr)free - (uintptr)fifo_offband) / 8;
        fifo_offband_used[i] = 0;
        return 0;
    }
    uint32 i;
    for(i = 0; i < CNOS_PE_FIFOSLOTS; i++){
        if(fifo_offband_used[i] == 0){
            fifo_offband_used[i] = 1;
            fifo_offband[i] = 0;
            return fifo_offband + i;
        }
    }
    dbgerr("we ran out of fifo slots - CNOS_PE_FIFOSLOTS\n");
    return 0;
}
