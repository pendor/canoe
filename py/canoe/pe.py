import thshell.monitor

CHANNEL_SIZE = 24


class ChannelMonitor:
    mtyp = "pechannel"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+CHANNEL_SIZE-1]
        
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'peer' : 0,
            'peer2' : 4,
            'uid' : 8,
            'ff' : 12,
            's_entry' : 16
        }
        if key not in attr:
            raise AttributeError
        data = self.raw.get32(self.addr + attr[key])
        if key == 'peer':
            return (data, self.peer2)
        if key == 'ff':
            return thshell.monitor.FifoMonitor(self.mod, self.mod.addr_i2e(data))
        return data 

    def dump(self):
        print "channel from %s @ %x" % (self.mod.name, self.addr)
        print "  id: %d" % self.uid
        print "  peer: %d.%d" % self.peer
        fifo = self.ff
        print "  fifo: @%x" % fifo.addr
        print "  s_entry: %d" % self.s_entry

