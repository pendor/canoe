from .tools import getroot
from os.path import join

LIBS = {'canoe' : 'canoe',
        'beth' : 'backend/thlib',
        'bem3' : 'backend/m3',
        'app' : 'os/app',
        'cm' : 'os/cm',
        'pe' : 'os/pe'
       }

def getpath(name):
    return join(getroot(), LIBS[name])

