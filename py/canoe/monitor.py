import struct
import pprint

class CanoeMonitor(object):
    def __init__(self):
        self.nodes = {}

    def message(self, mod, msg):
        if msg[2] == 0x50 and msg[3] == 0xd3:
            adr = mod.thadr
            if not adr in self.nodes:
                self.nodes[adr] = {}
            self.parse(adr, msg)
            return True
        return False

    def finish(self):
        if len(self.nodes) == 0:
            return True
        print "CANOE objects"
        pprint.pprint(self.nodes)
        return True

    def parse(self, mod, msg):
        cmd, addr = struct.unpack("<II", msg[0:8])
        obj = {'type' : cmd, 'addr' : addr}
        self.nodes[mod][addr] = obj
        if cmd == 0xd35065b4: #CNNOD_MANAGER
            obj['type'] = "CNNOD_MAN"
            #peers
            nobjs, = struct.unpack("<I", msg[8:12])
            obj['peers_s'] = nobjs
            obj['peers'] = {}
            pos = 12
            for i in range(nobjs):
                addr, = struct.unpack("<I", msg[pos+i*4:pos+4+i*4])
                obj['peers'][addr] = {'addr' : addr}
            pos += nobjs * 4
            #objs
            nobjs, = struct.unpack("<I", msg[pos:pos+4])
            obj['objs_s'] = nobjs
            obj['objs'] = {}
            obj['type'] = "CNNOD_MAN"
            pos += 4
            for i in range(nobjs):
                typ, addr = struct.unpack("<II", msg[pos+i*8:pos+8+i*8])
                obj['objs'][addr] = {'type' : cnman_objtype(typ), 'addr' : addr}
        elif cmd == 0xd3502b1f: #CNNOD_OBJECT
            print "CANOE monitor: object!"
            obj['type'] = "CNMAN_OBJECT"
            cano, uid, goi, typ = struct.unpack("<IIII", msg[8:24])
            obj['cano'] = cano
            obj['uid'] = uid
            obj['goi'] = goi
            obj['type'] = cnman_objtype(typ)
        else:
            print "CANOE monitor: cannot parse command: %#x" % cmd

def cnnod_objtype(inttyp):
    types = ['not_invalid', 'not_binary', 'not_task', 'not_buffer',
             'not_transfer', 'not_thread']
    try:
        return types[inttyp]
    except IndexError:
        return 'bad value: %d' % inttyp

def cnman_objtype(inttyp):
    types = ["mot_empty", "mot_binary", "mot_channel", "mot_canoe", "mot_peer",
             "mot_exec", "mot_block", "mot_buffer"]
    try:
        return types[inttyp]
    except IndexError:
        return 'bad value: %d' % inttyp
