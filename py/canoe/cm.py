CANOE_SIZE = 24
PROCESS_SIZE = 32
OBJECT_SIZE = 8
CHANNEL_SIZE = 72
PRIMITIVE_SIZE = 24
BINARY_SIZE = 48
PE_SIZE = 40

def getobj(mod, typ, ptr):
    if ptr == 0:
        return None
    if typ == 3: #process
        return ProcessMonitor(mod, ptr)
    if typ == 2: #channel
        return ChannelMonitor(mod, ptr)
    if typ == 5:
        return PrimitiveMonitor(mod, ptr)
    if typ == 1:
        return BinaryMonitor(mod, ptr)
    print "unknown obj type: %d" % typ
    return None

class CanoeMonitor:
    mtyp = "canoe"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None
        self.raw_obj = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+CANOE_SIZE-1]
        adr = self.mod.addr_i2e(self.obj)
        self.raw_obj = self.mod.mem[adr:adr + OBJECT_SIZE * self.obj_n - 1] 
       
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'desc_uid' : 0,
            'desc_type' : 4,
            'obj_s' : 8,
            'obj_n' : 12,
            'obj' : 16,
            'f_deploying' : 20
        }
        if key not in attr:
            raise AttributeError(key)
        data = self.raw.get32(self.addr + attr[key])
        return data

    def getobj(self, n):
        if n >= self.obj_n:
            return None
        base = self.mod.addr_i2e(self.obj)
        typ = self.raw_obj.get32(base + OBJECT_SIZE * n) 
        ptr = self.raw_obj.get32(base + OBJECT_SIZE * n + 4)
        ptr = self.mod.addr_i2e(ptr) 
        return getobj(self.mod, typ, ptr)

    def dump(self):
        print "canoe from %s @ %x" % (self.mod.name, self.addr)
        print "  id: %d" % self.desc_uid
        print "  obj: %x %d/%d" % (self.obj, self.obj_n, self.obj_s)

class ProcessMonitor:
    mtyp = "canoeprocess"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+PROCESS_SIZE-1]
        
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'desc_canoe' : 0,
            'desc_uid' : 4,
            'desc_core' : 8,
            'desc_params' : 12,
            'desc_s_params' : 16,
            'desc_map' : 20,
            'binary' : 24,
            'pestate' : 28,
            'flags' : 32,
        }
        if key not in attr:
            raise AttributeError
        data = self.raw.get32(self.addr + attr[key])
        if key == 'pestate':
            return PeMonitor(self.mod, self.mod.addr_i2e(data))
        return data 

    def dump(self):
        print "process from %s @ %x" % (self.mod.name, self.addr)
        print "  id: %d.%d" % (self.desc_canoe, self.desc_uid)
        pe = self.pestate
        print "  pe: %d:%d" % pe.adr

class ChannelMonitor:
    mtyp = "canoechannel"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+CHANNEL_SIZE-1]
        
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'desc_canoe' : 0,
            'desc_uid' : 4,
            'desc_src' : 8,
            'desc_dst' : 12,
            'desc_s_entry' : 16,
            'desc_s_fifo' : 20,
            'desc_s_wrap' : 24,
            'desc_balance' : 28,
            'desc_f_pulling' : 28,
            'sender' : 32,
            'sender2' : 36,
            'receiver' : 40,
            'receiver2' : 44,
            'send_noty' : 48,
            'recv_noty' : 52,
            'global' : 56,
            's_global' : 60,
            'flags' : 64,
        }
        if key not in attr:
            raise AttributeError
        data = self.raw.get32(self.addr + attr[key])
        if key == 'sender':
            return getobj(self.mod, data, self.mod.addr_i2e(self.sender2))
        if key == 'receiver':
            return getobj(self.mod, data, self.mod.addr_i2e(self.receiver2))
        return data 

    def dump(self):
        print "channel from %s @ %x" % (self.mod.name, self.addr)
        print "  id: %d.%d" % (self.desc_canoe, self.desc_uid)
        print "  %d -> %d" % (self.desc_src, self.desc_dst)
        print "  size: e:%d f:%d w:%d" % (self.desc_s_entry, self.desc_s_fifo, self.desc_s_wrap)
        sender = self.sender
        if sender:
            print "  sender: %d.%d" % (sender.desc_canoe, sender.desc_uid)
        receiver = self.receiver
        if receiver:
            print "  receiver: %d.%d" % (receiver.desc_canoe, receiver.desc_uid)

class PrimitiveMonitor:
    mtyp = "canoeprimitive"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+PRIMITIVE_SIZE-1]
        
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'desc_canoe' : 0,
            'desc_uid' : 4,
            'desc_type' : 8,
            'desc_start' : 12,
            'desc_s_max' : 16,
        }
        if key not in attr:
            raise AttributeError
        data = self.raw.get32(self.addr + attr[key])
        return data 

    def dump(self):
        print "primitive from %s @ %x" % (self.mod.name, self.addr)
        print "  id: %d.%d" % (self.desc_canoe, self.desc_uid)
        print "  @%x (%d)" % (self.desc_start, self.desc_s_max)

class BinaryMonitor:
    mtyp = "canoebinary"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+BINARY_SIZE-1]
        
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'desc_canoe' : 0,
            'desc_core' : 4,
            'desc_bin' : 8,
            'desc_bin_s' : 12,
            'desc_entry' : 16,
            'desc_gbin' : 20,
            'desc_gbin_s' : 24,
            'desc_glob' : 28,
            'desc_glob_s' : 32,
            'desc_md50' : 36,
            'desc_md51' : 40,
            'pes_deployed' : 44
        }
        if key not in attr:
            raise AttributeError
        data = self.raw.get32(self.addr + attr[key])
        return data 

    def dump(self):
        print "binary from %s @ %x" % (self.mod.name, self.addr)
        print "  id: %d.%d" % (self.desc_canoe, self.desc_core)
        print "  pes: %x" % self.pes_deployed

class PeMonitor:
    mtyp = "canoepe"
    def __init__(self, mod, addr):
        self.mod = mod
        self.addr = addr
        self.raw = None

    def fetchstatus(self):
        self.raw = self.mod.mem[self.addr:self.addr+PE_SIZE-1]
        
    def __getattr__(self, key):
        if not self.raw:
            self.fetchstatus()
        attr = {
            'adr' : 0,
            'adr2' : 4,
            'noty' : 8,
            'nnoty' : 12,
            'snoty' : 16,
            'ffdn' : 20,
            'ffup' : 24,
        }
        if key not in attr:
            raise AttributeError
        data = self.raw.get32(self.addr + attr[key])
        if key == 'adr':
            return (data, self.adr2)
        return data 

    def dump(self):
        print "PE from %s @ %x" % (self.mod.name, self.addr)
        print "  adr: %d:%d" % self.adr
