import inspect
import os

def getroot():
    "returns the canoe base directory"
    cframe = inspect.currentframe()
    cfile = inspect.getfile(cframe)
    cpath = os.path.abspath(cfile)
    cdir = os.path.dirname(cpath)
    cpy = os.path.split(cdir)[0]
    croot = os.path.split(cpy)[0]
    return croot
