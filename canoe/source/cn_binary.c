#include "config_canoe.h"
#define CANOE_LOGLEVEL  CN_BINARY_LOGLEVEL
#include "canoe.h"

uint8 cn_binary_deploy(cn_binary_desc_p bin, cn_peer_p pe)
{
    if(pe->currbin == bin)
        return 0;

    uint32 *p = cn_poke(pe->ffdn, CN_BINARY_DESC_STORESIZE + 8);
    if(!p)
        return 1;
    cnlog("bin : sending binary to peer:%s\n", pe->name);
    p[0] = CN_BINARY_DESC_STORESIZE + 4;
    p[1] = cwc_load;
    cn_binary_desc_store(bin, p + 2);
    cn_push(pe->ffdn);
    
    pe->currbin = bin;

    cnlog3("bin : binary:%#x to peer:%s\n", bin->core, pe->name);
    return 0;
}

cn_binary_desc_p cn_binary_new(void *data)
{
    cn_binary_desc_p desc = cn_malloc(sizeof(struct cn_binary_desc_t));
    cn_binary_desc_load(desc, data);
    cnlog3("bin : binary created - uid:%d @%#x(%#x)\n", desc->uid, desc,
                                               sizeof(struct cn_binary_desc_t));
    return desc;
}

void cn_binary_keycpy(cn_binary_desc_p bin, cstr key)
{
    cn_memcpy(bin->key, key, CN_BINARY_KEYLEN);
}

uint32 cn_binary_desc_store(cn_binary_desc_p d, void *data)
{
    uint32 *p = (uint32 *)data;
    p[0] = d->uid;
    p[1] = d->core;
    p[2] = d->typ;
    p[3] = d->param;
    cn_memcpy(p + 4, d->key, CN_BINARY_KEYLEN);
    return CN_BINARY_DESC_STORESIZE;
}

uint32 cn_binary_desc_load(cn_binary_desc_p d, void *data)
{
    uint32 *p = (uint32 *)data;
    d->uid = p[0];
    d->core = p[1];
    d->typ = p[2];
    d->param = p[3];
    cn_memcpy(d->key, p + 4, CN_BINARY_KEYLEN);
    return CN_BINARY_DESC_STORESIZE;
}

