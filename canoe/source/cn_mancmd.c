#include "canoe.h"

void cn_buffer_desc_store(cn_buffer_desc_p d, void *data)
{
    uint32 *p = (uint32 *)data;
    p[0] = d->uid;
    p[1] = d->size;
    p[2] = d->maxacc;
    p[3] = d->f_block | d->f_channel << 1;
    p[4] = d->nref;
}

void cn_buffer_desc_load(cn_buffer_desc_p d, void *data)
{
    uint32 *p = (uint32 *)data;
    d->uid = p[0];
    d->size = p[1];
    d->maxacc = p[2];
    d->f_block = p[3] & 0x1 ? 1 : 0;
    d->f_channel = p[3] & 0x2 ? 1 : 0;
    d->nref = p[4];
}

void cn_request_desc_store(cn_request_desc *d, void *data)
{
    uint32 *p = (uint32 *)data;
    p[0] = d->gen.p1;
    p[1] = d->gen.p2;
}

void cn_request_desc_load(cn_request_desc *d, void *data)
{
    uint32 *p = (uint32 *)data;
    d->gen.p1 = p[0];
    d->gen.p2 = p[1];
}
