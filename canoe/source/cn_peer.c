#include "config_canoe.h"
#define CANOE_LOGLEVEL  CN_PEER_LOGLEVEL
#include "canoe.h"

void _peer_recvref(cn_peer_p peer, cn_manager_p man, void *data);
void _peer_recvunref(cn_peer_p peer, cn_manager_p man, void *data);
void _peer_recvfinnotice(cn_manager_p man, void *data);

cn_peer_p cn_peer_new(cn_io dn, cn_io up)
{
    cn_peer_p nod = cn_malloc(sizeof(struct cn_peer_t));
    cn_memset(nod, 0, sizeof(struct cn_peer_t));
    nod->handle = 0;
    nod->ffdn = dn;
    nod->ffup = up;
    nod->name = "NA";
    cnlog2("peer: peer created @%#x\n", nod);
    return nod;
}

void cn_peer_work(cn_peer_p nod, cn_manager_p man)
{
    if(cn_receive(nod->ffup, &nod->ffup_msg)){
        //cnlog("receive! len:%d\n", nod->ffup_msg.length);
        uint32 *data = (uint32 *)nod->ffup_msg.data;
        enum cn_mancmd_t cmd = data[0];
        struct cn_object_t obj;
        switch(cmd){
            case cmc_handle:{
                cn_handle_p hand = cn_malloc(CN_HANDLE_STRUCTSIZE);
                cn_handle_load(hand, data + 1);
                cnlog3("peer: got handle from node %s @%#x\n", nod->name, hand);
                nod->handle = hand;
                nod->f_waithandle = 0;
                break;
            }
            case cmc_binary:
                obj.obj.bin = cn_binary_new(data + 1);
                obj.typ = cot_binary;
                cn_manager_addobj(man, &obj);
                break;
            case cmc_exec:
                obj.obj.exec = cn_exec_new(data + 1, man);
                obj.typ = cot_exec;
                cn_manager_addobj(man, &obj);
                break;
            case cmc_buffer:
                cn_buffer_new(data + 1, &obj);
                cn_manager_addobj(man, &obj);
                break;
            case cmc_ref:
                _peer_recvref(nod, man, data + 1);
                break;
            case cmc_unref:
                _peer_recvunref(nod, man, data + 1);
                break;
            case cmc_finish:
                _peer_recvfinnotice(man, data + 1);
                break;
            default:
                cnerr("peer: received unknown command:%#x\n", cmd);
        }
        cn_received(nod->ffup);
    }
    if(nod->ref_req && nod->ref_req->f_avail){
        cnlog3("peer: try to deploy block:%#x by reference request peer:%s\n",
            nod->ref_req->uid, nod->name);
        if(!cn_block_deploy(nod->ref_req, nod, 0))
            nod->ref_req = 0;
    }
}

void _peer_recvref(cn_peer_p peer, cn_manager_p man, void *data)
{
    cn_request_desc desc;
    cn_request_desc_load(&desc, data);
    cnlog3("peer: buffer reference request uid:%d write:%d peer:%s\n",
        desc.ref.buffer, desc.ref.f_write, peer->name);
    cn_object_p o;
    //try block
    o = cn_manager_getobj_id(man, desc.ref.buffer);
    if(!o)
        cnerr("peer: cannot find buffer for referencing\n");
    //deploy buffer to peer
    if(o->typ == cot_block){    
        if(desc.ref.f_write){
            if(cn_block_deploy(o->obj.block, peer, 1))
                cnerr("peer: deploy failt for reference write request\n");
            o->obj.block->f_avail = 1;
        }else
            peer->ref_req = o->obj.block;
        return;
    }
    if(o->typ == cot_channel){
        cnerr("peer: channel ref not implemented\n");
        return;
    }
    cnerr("peer: cannot reference object of typ: %d\n", o->typ);
    return;
}

void _peer_recvunref(cn_peer_p peer, cn_manager_p man, void *data)
{
    cn_request_desc desc;
    cn_request_desc_load(&desc, data);
    cnlog3("peer: buffer reference request uid:%d write:%d\n", desc.ref.buffer, desc.ref.f_write);
    cn_object_p o;
    //try block
    o = cn_manager_getobj_id(man, desc.ref.buffer);
    if(!o)
        cnerr("peer: cannot find buffer for un-referencing\n");
    //deploy buffer to peer
    if(o->typ == cot_block){    
        cn_block_unref(o->obj.block);
        return;
    }
    if(o->typ == cot_channel){
        cnerr("peer: channel ref not implemented\n");
        return;
    }
    cnerr("peer: cannot reference object of typ: %d\n", o->typ);
    return;
}

void _peer_recvfinnotice(cn_manager_p man, void *data)
{
    cn_request_desc desc;
    cn_request_desc_load(&desc, data);
    cn_object_p obj = cn_manager_getobj_id(man, desc.obj.uid);
    cn_exec_finish(obj->obj.exec);
}

cn_handle_p cn_peer_gethandle(cn_peer_p nod)
{
    if(!nod->f_waithandle){
        uint32 *p = cn_poke(nod->ffdn, 8);
        if(p){
            p[0] = 4;
            p[1] = cwc_handle_request;
            cn_push(nod->ffdn);
            nod->f_waithandle = 1;
            cnlog3("peer: requested handle from:%s\n", nod->name);
        }
    }
    if(nod->handle){
        cn_handle_p tmp = nod->handle;
        nod->handle = 0;
        return tmp;
    }else
        return 0;
}

/*void cnman_peer_dump(cnman_peer *peer, uint8 deep)
{
    uint32 data = CNMAN_PEER;
    cnmsg(&data, 4);
    data = (uintptr)peer;
    cnmsg(&data, 4);
    data = cn_adr2uint(peer->adr);
    cnmsg_last();
    cnmsg(&data, 4);
}
*/
