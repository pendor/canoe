#include "config_canoe.h"
#define CANOE_LOGLEVEL  CN_MANAGER_LOGLEVEL
#include "canoe.h"

#ifdef CN_MANAGER_CHECKS
#define CHK 
#endif

#ifndef CN_MANAGER_OBJECT_SLOTS
#define CN_MANAGER_OBJECT_SLOTS 100
#endif

#ifndef CN_MANAGER_PEER_SLOTS
#define CN_MANAGER_PEER_SLOTS 15
#endif

/*struct _cnman_working_state_t{
    enum {
        working_free = 0,
        block_deploy
    }                       mode;
    uint32                  state;
    union {
        struct {
            cnman_block *   block;
            cnman_peer *    node;
            cn_handle_p     sender_handler;
            cn_handle_p     recvr_handler;
            uint32          slot;
        };
    };
} _cnman_working_state;*/

/*cnman_channel * _cnman_channel_new(cnman_man *man, cn_buffer_desc *);
cnman_block * _cnman_block_new(cnman_man *man, cn_buffer_desc *desc);
void        _cnman_buffer_reference(cnman_man *, cnman_peer *, cn_request_desc *);
void        _cnman_request(cnman_man *man, cnman_peer *peer, void *data);
uint8       _cnman_working();*/

/////////////////////////////////////////////
//
// MANAGER
//

cn_manager_p cn_manager_new(){
    uint32 size = sizeof(struct cn_manager_t);
    size += sizeof(struct cn_object_t) * CN_MANAGER_OBJECT_SLOTS;
    size += sizeof(cn_peer_p) * CN_MANAGER_PEER_SLOTS;
    cn_manager_p man = cn_malloc(size);
    man->obj = (cn_object_p)(man + 1);
    man->peer = (cn_peer_p *)(man->obj + CN_MANAGER_OBJECT_SLOTS);
    man->uidsource = 0;
    uint32 i;
    for(i = 0; i < CN_MANAGER_OBJECT_SLOTS; i++)
        man->obj[i].typ = cot_empty;
    for(i = 0; i < CN_MANAGER_PEER_SLOTS; i++)
        man->peer[i] = 0;
    cnlog2("man : manager created @%#x\n", man);
    return man;
}

void cn_manager_addpeer(cn_manager_p man, cn_peer_p n)
{
    uint32 i;
    for(i = 0; i < CN_MANAGER_PEER_SLOTS; i++){
        if(man->peer[i] == 0){
            man->peer[i] = n;
            return;
        }
    }
    cnerr("manager full - no more peers - size:%d\n", CN_MANAGER_PEER_SLOTS);
}

void cn_manager_work(cn_manager_p man)
{
    uint32 i;
    //work on objects
    for(i = 0; i < CN_MANAGER_OBJECT_SLOTS; i++){
        if(man->obj[i].typ != cot_empty)
            cn_object_work(man->obj + i, man);
    }
    //work on peers
    for(i = 0; i < CN_MANAGER_PEER_SLOTS; i++){
        if(man->peer[i] != 0)
            cn_peer_work(man->peer[i], man);
    }
}

void cn_manager_remobj_id(cn_manager_p man, cn_id uid)
{
    uint32 n;
    for(n = 0; n < CN_MANAGER_OBJECT_SLOTS; n++){
        if(*man->obj[n].obj.id == uid){
            man->obj[n].typ = cot_empty;
            return;
        }
    }
    cnerr("cannot remove object from cano (not found)\n");
}

void cn_manager_addobj(cn_manager_p man, cn_object_p obj)
{
    uint32 n;
    for(n = 0; n < CN_MANAGER_OBJECT_SLOTS; n++){
        if(man->obj[n].typ == cot_empty){
            man->obj[n].obj.vs = obj->obj.vs;
            man->obj[n].typ = obj->typ;
            return;
        }
    }
    cnerr("cannot add object to manager (full) %d\n", CN_MANAGER_OBJECT_SLOTS);
}

cn_object_p cn_manager_getobj_id(cn_manager_p man, cn_id uid)
{
    uint32 i;
    for(i = 0; i < CN_MANAGER_OBJECT_SLOTS; i++){
        if(*man->obj[i].obj.id == uid)
            return man->obj + i;
    }
    cnerr("cannot find object uid:%d\n", uid);
    return 0;
}

cn_binary_desc_p cn_manager_getbin_core (cn_manager_p man, cn_id core)
{
    for(uint32 n = 0; n < CN_MANAGER_OBJECT_SLOTS; n++){
        if(man->obj[n].typ != cot_binary)
            continue;
        if(man->obj[n].obj.bin->core == core)
            return man->obj[n].obj.bin;
    }
    return 0;
}

uint32 cn_manager_peer_slots()
{
    return CN_MANAGER_PEER_SLOTS;
}

void cn_object_work(cn_object_p obj, cn_manager_p man)
{
    switch(obj->typ){
    case cot_exec:
        cn_exec_work(obj->obj.exec, man);
        break;
    case cot_block:
        cn_block_work(obj->obj.block, man);
        break;
    default:
        break;
    }
}



/*

void _cnman_request(cnman_man *man, cnman_peer * peer, void *data)
{
    cnerr("not implemented\n");
}

void cnman_manager_dump(cnman_man *man, uint8 deep)
{
    uint32 data = CNMAN_MANAGER;
    cnmsg(&data, 4);
    data = (uintptr)man;
    cnmsg(&data, 4);
    cnmsg(&man->peers_s, 4);
    uint32 i;
    for(i = 0; i < man->peers_s; i++)
        cnmsg(&man->peers[i], 4);
    cnmsg(&man->objs_s, 4);
    for(i = 0; i < man->objs_s; i++){
        cnmsg(&man->objs[i].typ, 4);
        data = (uintptr)mod_int2ext(&man->objs[i].vs);
        cnmsg(&data, 4);
    }
    cnmsg_last();
    cnmsg(&data, 1);
    if(deep){
        if(deep >= 1)
            deep -= 1;
        for(i = 0; i < man->peers_s; i++){
            if(man->peers[i])
                cnman_peer_dump(man->peers[i], deep);
        }
        for(i = 0; i < man->objs_s; i++){
            if(man->objs[i].typ != mot_empty)
                cnman_object_dump(man->objs + i);
        }
    }
}

*/


/////////////////////////////////////////////
//
// CANOE
//

/*cn_id cnman_canoe_new(cnman_man *man, void *data)
{
    uint32 *d = (uint32 *)data;
    uint32 uid = d[0];
    if(uid == 0)
        uid = ++man->goisource;
    cnlog1("cnman : new canoe created; id:%d\n", uid);
    return uid;
}*/

/*void cnman_object_dump(cnman_object *obj)
{
    uint32 data = CNMAN_OBJECT;
    cnmsg(&data, 4);
    data = (uintptr)obj;
    cnmsg(&data, 4);
    cnmsg(&obj->oid->cano, 4);
    cnmsg(&obj->oid->uid, 4);
    cnmsg(&obj->oid->goi, 4);
    cnmsg_last();
    data = obj->typ;
    cnmsg(&data, 4);
}

uint32 cnman_manager_objectspace(cnman_man *man)
{
    uint32 i, accu = 0;
    for(i = 0; i < man->objs_s; i++){
        if(man->objs[i].typ == mot_empty)
            accu += 1;
    }
    return accu;
}

*/

/////////////////////////////////////////////
//
// EXEC
//



/*
void cnman_exec_setfinished(cnman_exec *exe)
{
    exe->f_finished = 1; //synchronous task execution
    uint32 i;
    for(i = 0; i < exe->n_ios; i++){
        if(!exe->ios[i].f_channel){//block
            if(exe->ios[i].block->creator == exe){
                exe->ios[i].block->f_avail = 1;
                cnlog4("cnman : block:%#x becomes available\n", exe->ios[i].block->uid);
            } 
        }//else //channel
    }
}

void cnman_exec_recvfin(cnman_man *man, void *data)
{
    uint32 *d = (uint32 *)data;
    uint32 goi = d[0];
    cnman_object *obj = cnman_manager_get(man, goi);
    cnman_exec_setfinished(obj->exec);
}*/

/*
uint8 _cnman_working()
{
    struct _cnman_working_state_t *s = &_cnman_working_state;
    uint8 ret;
    switch(s->mode){
    case working_free:
        return 0;
    case block_deploy:
        ret = _cnman_working_block_deploy();
        break;
    }
    s->state = ret;
    if(ret == 1)
        s->mode = working_free;
    return ret;
}
*/



