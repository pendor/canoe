#include "config_canoe.h"
#define CANOE_LOGLEVEL CN_EXEC_LOGLEVEL
#include "canoe.h"

void _exec_place(cn_exec_p , cn_manager_p);
void _exec_findbin(cn_exec_p , cn_manager_p);
void _exec_checkdep(cn_exec_p );
void _exec_deploybin(cn_exec_p );
void _exec_iosdeploy(cn_exec_p );
void _exec_deploy(cn_exec_p );
void _exec_iosclean(cn_exec_p );
void _exec_del(cn_exec_p , cn_manager_p);

cn_exec_p  cn_exec_new(void *data, cn_manager_p man)
{
    struct cn_exec_desc_t desc;
    uint32 pos;
    pos = cn_exec_desc_load(&desc, data);
    cn_exec_p p = cn_malloc(sizeof(struct cn_exec_t));
    cn_memset(p, 0, sizeof(struct cn_exec_t));
    p->uid = desc.uid;
    p->core = desc.core;
    p->place = desc.place;
    uint32 n;
    struct cn_assign_t assign;
    for(n = 0; n < desc.n_assigns; n++){
        cn_assign_load(&assign, n, data + pos);
        cnlog("man : find io %d/%d for new process id:%d io:%d sink:%d\n",
            n+1, desc.n_assigns, desc.uid, assign.buffer, assign.f_sink);
        cn_object_p obj;
        obj = cn_manager_getobj_id(man, assign.buffer);
        cnlog("man : ...got object @ %#x\n", obj->obj.vs);
        if(obj->typ == cot_block)
            cn_block_use(obj->obj.block, p, assign.f_sink);
        else
            cn_channel_use(obj->obj.chan, p, assign.f_sink);
    }
    p->f_noty_finished = desc.f_noty_finished;
    cnlog2("man : new exec created; id:%d @%#x\n", p->uid, p);
    return p;
}

void cn_exec_work(cn_exec_p tsk, cn_manager_p man)
{
    if(!tsk->node)
        _exec_place(tsk, man);
    if(!tsk->bin)
        _exec_findbin(tsk, man);
    if(!tsk->f_nodepend)
        _exec_checkdep(tsk);
    if(tsk->f_nodepend && tsk->node && tsk->bin && !tsk->f_bindeployed)
        _exec_deploybin(tsk);
    if(tsk->f_nodepend && tsk->node && !tsk->f_iosdeployed)
        _exec_iosdeploy(tsk);
    if(!tsk->f_deployed && tsk->f_iosdeployed & tsk->f_bindeployed)
        _exec_deploy(tsk);
    if(!tsk->f_ioscleaned && tsk->f_finished)
        _exec_iosclean(tsk);
    if(tsk->f_ioscleaned)
        _exec_del(tsk, man);
}

void cn_exec_finish(cn_exec_p exec)
{
    cnlog("exec: task finished uid:%d\n", exec->uid);
    exec->f_finished = 1;
}

void _exec_findbin(cn_exec_p task, cn_manager_p man)
{
    cn_binary_desc_p bin;

    bin = cn_manager_getbin_core(man, task->core);
    if(!bin)
        cnerr("exec: cannot find matching binar for task:%d core:%d\n", task->uid, task->core);
    task->bin = bin;
    cnlog3("man : task %#x linked binary\n", task->uid);
    return;
}

void _exec_checkdep(cn_exec_p task)
{
    uint32 i;
    for(i = 0; i < task->n_ios; i++){
        if(task->ios[i].f_channel)
            continue;
        cn_block_p b = task->ios[i].io.block;
        if(!b->f_avail && b->creator != task)
            return;
    }
    cnlog3("man : task %#x is free of dependencies\n", task->uid);
    task->f_nodepend = 1;
}

void _exec_deploybin(cn_exec_p task)
{
    if(!cn_binary_deploy(task->bin, task->node)){
        task->f_bindeployed = 1;
        cnlog3("man : exec %#x deployed binary\n", task->uid);
    }
}

void _exec_iosdeploy(cn_exec_p task)
{
    uint32 n;
    for(n = 0; n < task->n_ios; n++){
        if(task->ios[n].f_channel){
            if(cn_channel_deploy(task->ios[n].io.chan))
                return;
        }else{
            cn_block_p b = task->ios[n].io.block;
            uint8 creator = b->creator == task;
            if(cn_block_deploy(b, task->node, creator))
                return;
        }
    }
    task->f_iosdeployed = 1;
    cnlog3("man : task %#x deployed all IOs\n", task->uid);
}

void _exec_deploy(cn_exec_p task)
{
    #define LEN 4 * 4
    uint32 *p = (uint32 *)cn_poke(task->node->ffdn, LEN + 4 + 4 * task->n_ios);
    if(!p)
        return;
    p[0] = LEN + 4 * task->n_ios;
    p[1] = cwc_exec;
    cn_run_desc desc;
    cn_runio_desc iodesc[task->n_ios];
    desc.uid = task->uid;
    desc.nios = task->n_ios;
    desc.f_noty_finish = task->f_noty_finished;
    desc.ios = iodesc;
    uint32 n;
    for(n = 0; n < task->n_ios; n++){
        iodesc[n].uid = task->ios[n].io.block->uid;
        iodesc[n].f_write = task->ios[n].io.block->creator == task ? 1 : 0;
    }
    cn_run_desc_store(&desc, p + 2);
    #undef LEN
    cn_push(task->node->ffdn);
    task->f_deployed = 1;
    //if(task->f_synchronous)
    //    cnman_exec_setfinished(exe);
    cnlog3("man : exec %#x deployed to peer:%s\n", task->uid, task->node->name);
}

void _exec_iosclean(cn_exec_p task)
{
    uint32 i;
    for(i = 0; i < task->n_ios; i++){
        if(task->ios[i].f_channel) //FIXME clean channels
            continue;
        if(task->ios[i].io.block->creator == task){
            task->ios[i].io.block->creator = 0;
            task->ios[i].io.block->f_avail = 1;
        }
        if(task->ios[i].io.block->ref < 1){
            cnlog("man : WARNING - block ref dropped below 0 - we will ignore that\n");
            continue;
        }
        task->ios[i].io.block->ref -= 1;
        if(task->ios[i].io.block->ref == 0)
            cnlog3("man : exec %#x block %#x ready for destruction\n", task->uid, task->ios[i].io.block->uid);
        else
            cnlog3("man : exec %#x block %#x ref @ %d\n", task->uid, task->ios[i].io.block->uid, task->ios[i].io.block->ref);
    }
    task->f_ioscleaned = 1;
    cnlog3("man : exec %#x cleaned\n", task->uid);
}

void _exec_del(cn_exec_p task, cn_manager_p man)
{
    cn_manager_remobj_id(man, task->uid);
    cn_free(task);
    cnlog3("cnman : exec %#x killed\n", task->uid);
}

uint32 cn_exec_desc_store(cn_exec_desc_p d, void *data)
{
    uint32 *p = (uint32 *)data;
    p[0] = d->uid;
    p[1] = d->core;
    p[2] = d->n_assigns;
    p[3] = d->f_noty_finished;
    p[4] = d->place;
    return CN_EXEC_STORESIZE;
}

uint32 cn_exec_desc_load(cn_exec_desc_p d, void *data)
{
    uint32 *p = (uint32 *)data;
    d->uid = p[0];
    d->core = p[1];
    d->n_assigns = p[2];
    d->f_noty_finished = p[3];
    d->place = p[4];
    return CN_EXEC_STORESIZE;
}

uint32 cn_assign_store(cn_assign_p d, uint32 num, void *data)
{
    uint32 i, *p = (uint32 *)data;
    for(i = 0; i < num; i++)
        p[i] = (d[i].f_sink ? 0x80000000 : 0) | d[i].buffer;
    return CN_ASSIGN_STORESIZE * num;
}

uint32 cn_assign_load(cn_assign_p d, uint32 pos, void *data)
{
    uint32 *p = (uint32 *)data;
    d->f_sink = p[pos] & 0x80000000 ? 1 : 0;
    d->buffer = p[pos] & 0x0fffffff;
    return CN_ASSIGN_STORESIZE * (pos + 1);
}

