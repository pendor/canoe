#include "config_canoe.h"
#define CANOE_LOGLEVEL CN_KERN_LOGLEVEL
#include "canoe.h"

cn_worker *     cn_kern_worker = 0;
cn_call         cn_kern_hook = 0;

void cn_kern_setkernel(cn_call k)
{
    cn_kern_hook = k;
}

void cn_kern_setworker(cn_worker *w)
{
    cn_kern_worker = w;
}

void cn_kern_run()
{
    if(cn_kern_hook == 0)
        cnerr("kern: ERROR there is no kernel defined that we can run\n");
    cnlog3("kern: run kernel @%#p\n", cn_kern_hook);
    uint32 ret = cn_kern_hook();
    cnlog2("kern: kernel return - s:%d x:%#x u:%u\n", ret, ret, ret);
    ret++;
}

cn_io cn_kern_getio(uint32 num)
{
    return cn_kern_worker->io[num];
}

void cn_kern_wait(){
    cn_worker_work(cn_kern_worker);
}

//sending a binary description with globlas over a given fifo
void cn_kern_addbin(cn_binary_desc_p d)
{
    #define LEN CN_BINARY_DESC_STORESIZE
    uint32 *p = 0;
    while(0 == (p = cn_poke(cn_kern_worker->io_up, LEN + 8)))
        cn_kern_wait();
    p[0] = LEN + 4;
    p[1] = cmc_binary;
    cn_binary_desc_store(d, p + 2);
    #undef LEN
    cn_push(cn_kern_worker->io_up);
    cnlog4("kern: sent binary\n");
}

//sending a process description over a given fifo
void cn_kern_addexec(cn_exec_desc_p d, cn_assign_p a)
{
    #define LEN CN_EXEC_STORESIZE + CN_ASSIGN_STORESIZE * nassigns
    uint32 nassigns = d->n_assigns;
    void *p = 0;
    uint32 pos = 0;
    while(0 == (p = cn_poke(cn_kern_worker->io_up, LEN + 8)))
        cn_kern_wait();
    ((uint32 *)p)[0] = LEN + 4;
    ((uint32 *)p)[1] = cmc_exec;
    pos = 8;
    pos += cn_exec_desc_store(d, p + pos);
    pos += cn_assign_store(a, d->n_assigns, p + pos);
    #undef LEN
    cn_push(cn_kern_worker->io_up);
    cnlog4("kern: sent exec\n");
}

//sending a channel description over a given fifo
void cn_kern_addbuffer(cn_buffer_desc_p d)
{
    #define LEN 4 * 7
    uint32 *p = 0;
    while(0 == (p = cn_poke(cn_kern_worker->io_up, LEN + 4)))
        cn_kern_wait();
    p[0] = LEN;
    p[1] = cmc_buffer;
    cn_buffer_desc_store(d, p + 2);
    #undef LEN
    cn_push(cn_kern_worker->io_up);
    #ifdef CHK
        if(d->data && d->f_channel)
            cnerr("kern: init data on a channel does not work now - may in future\n");
    #endif
    if(d->data){
        cnlog4("kern: add data to buffer...\n");
        cn_io buf = cn_kern_ref(d->uid, 1);
        void *dst = 0;
        while(0 == (dst = cn_poke(buf, d->size)))
            cn_kern_wait();
        cn_memcpy(dst, d->data, d->size);
        cn_push(buf);
        cn_kern_unref(d->uid);
        cnlog4("kern: ...data added\n");
    }
    cnlog4("kern: sent buffer\n");
}

void _request(cn_worker *, enum cn_mancmd_t, cn_request_desc *);

cn_io cn_kern_ref(cn_id buffer, uint8 write)
{
    cn_request_desc req;
    req.ref.buffer = buffer;
    req.ref.f_write = write;
    _request(cn_kern_worker, cmc_ref, &req);
    cnlog4("kern: ref request sent for buf:%#x\n", buffer);
    cn_chunk *chk;
    while(0 == (chk = cn_worker_getchunk_id(cn_kern_worker, buffer)))
        cn_kern_wait();
    chk->f_write = write;
    cnlog4("kern: ref request: found chunk - wait for data\n");
    if(!write){
        while(!chk->f_valid)
            cn_kern_wait();
    }
    cnlog4("kern: ref request finished buf:%#x\n", chk->uid);
    return cn_access(chk->buf, !write); //FIXME memory leak?
}

void cn_kern_unref(cn_id uid)
{
    //set f_valid
    cn_chunk *chk;
    chk = cn_worker_getchunk_id(cn_kern_worker, uid);
    if(!chk)
        cnerr("kern: cannot unref - chunk not found\n");
    if(chk->f_write){
        chk->f_write = 0;
        chk->f_valid = 1;
    }
    //notify manager
    cn_request_desc req;
    req.ref.buffer = uid;
    _request(cn_kern_worker, cmc_unref, &req);
}

void _request(cn_worker *com, enum cn_mancmd_t cmd, cn_request_desc *desc)
{
    #define LEN 4 * 4
    uint32 *p;
    while(0 == (p = cn_poke(com->io_up, LEN + 4)))
        cn_worker_work(com);
    p[0] = LEN;
    p[1] = cmd;
    cn_request_desc_store(desc, p + 2);
    #undef LEN
    cn_push(com->io_up);
}

