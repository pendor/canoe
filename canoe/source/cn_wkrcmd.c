#include "canoe.h"

uint32 cn_wkr_request_store(cn_wkr_request *req, void *data)
{
    uint32 *d = (uint32 *)data;
    d[0] = req->gen.p1;
    d[1] = req->gen.p2;
    return 8;
}

uint32 cn_wkr_request_load(cn_wkr_request *req, void *data)
{
    uint32 *d = (uint32 *)data;
    req->gen.p1 = d[0];
    req->gen.p2 = d[1];
    return 8;
}

void cn_run_desc_store(cn_run_desc *desc, void *data)
{
    uint32 *d = (uint32 *)data;
    d[0] = desc->uid;
    d[1] = desc->nios;
    d[2] = desc->f_noty_finish;
    uint32 i;
    for(i = 0; i < desc->nios; i++)
        d[3+i] = (desc->ios[i].f_write ? 0x80000000 : 0) | desc->ios[i].uid;
}

void cn_run_desc_load(cn_run_desc *desc, void *data)
{
    uint32 *d = (uint32 *)data;
    desc->uid = d[0];
    desc->nios = d[1];
    desc->f_noty_finish = d[2];
}

void cn_run_desc_load_ios(cn_run_desc *desc, void *data)
{
    uint32 *d = (uint32 *)data;
    uint32 i;
    for(i = 0; i < desc->nios; i++){
        desc->ios[i].uid = d[3+i] & 0x0fffffff;
        desc->ios[i].f_write = d[3+i] & 0x80000000 ? 1 : 0;
    }
}

void cn_chan_desc_load(cn_chan_desc *desc, void *data)
{
    uint32 *d = (uint32 *)data;
    desc->uid = d[0];
    desc->size = d[1];
    desc->maxacc = d[2];
    desc->f_sender = d[3];
}

void cn_chan_desc_store(cn_chan_desc *desc, void *data)
{
    uint32 *d = (uint32 *)data;
    d[0] = desc->uid;
    d[1] = desc->size;
    d[2] = desc->maxacc;
    d[3] = desc->f_sender;
}
