#include "config_canoe.h"
#include "canoe.h"

#ifdef CN_BASE_CHECK
#define CHK 
#endif

#ifdef CHK
void _checkmsgsize(uint32 size)
{
    size -= 4;
    if(size > 200)
        cnerr("cn  : ERROR message header specifies message of %d bytes!?\n", size);
    if(size % 4)
        cnerr("cn  : ERROR message header specifies message not align at 4 (%d)\n", size);
}
#endif

uint8 cn_receive(cn_io io, struct cn_msg_t *msg)
{
    if(!msg->state){
        void *p = cn_peek(io, 4);
        if(p){
            msg->state = *(uint32 *)p;
            cn_pop(io);
            #ifdef CHK
                _checkmsgsize(msg->state);
            #endif
        }
    }
    if(msg->state){
        void *p = cn_peek(io, msg->state);
        if(p){
            msg->length = msg->state;
            msg->state = 0;
            msg->data = p;
            return 1;
        }
    }
    return 0;
}

void cn_received(cn_io io)
{
    cn_pop(io);
}

void cn_msg_dump(struct cn_msg_t *msg)
{
    if(msg->state){
        cnlog("cn  : message header; msg len:%d\n", msg->state);
        return;
    }
    cnlog("cn  : message len:%d\n", msg->length);
    uint32 *d = msg->data;
    for(uint32 i = 0 ; i < msg->length / 4; i++)
        cnlog("cn  : msg[%d] %#x %d\n", i, d[i], d[i]);
}