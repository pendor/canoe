#include "config_canoe.h"
#define CANOE_LOGLEVEL  CN_BUFFER_LOGLEVEL
#include "canoe.h"

void            _block_del(cn_block_p, cn_manager_p);
cn_block_p      _block_new(cn_buffer_desc_p desc);
cn_channel_p    _channel_new(cn_buffer_desc_p desc);

void cn_buffer_new(void *data, cn_object_p obj)
{
    cnlog3("buf : new buffer...\n");
    struct cn_buffer_desc_t desc;
    cn_buffer_desc_load(&desc, data);
    if(desc.f_block){
        obj->obj.block = _block_new(&desc);
        obj->typ = cot_block;
    }else{
        obj->obj.chan = _channel_new(&desc);
        obj->typ = cot_channel;
    }
}

/////////////////////////////////////////////
//
// BLOCK
//

void cn_block_work(cn_block_p block, cn_manager_p man)
{
    if(block->ref == 0){
        _block_del(block, man);
    }
}

void _block_del(cn_block_p block, cn_manager_p man)
{
    uint32 i, *p;
    for(i = 0; i < block->nchunks; i++){
        if(block->chunks[i] == 0)
            continue;
        #define LEN 4 * 3
        p = (uint32 *)cn_poke(block->chunks[i]->ffdn, LEN + 4);
        if(!p)
            return;
        cn_wkr_request req;
        req.block.uid = block->uid;
        p[0] = LEN;
        p[1] = cwc_block_del;
        cn_wkr_request_store(&req, p + 2);
        #undef LEN
        cn_push(block->chunks[i]->ffdn);
        block->chunks[i] = 0;
    }
    cnlog3("buf : block %#x (@%#x)chunks gone - remove from canoe!\n", block->uid, block);
    cn_manager_remobj_id(man, block->uid);
    cn_free(block);
}

uint8 cn_block_deploy(cn_block_p block, cn_peer_p nod, uint8 creator)
{
    //deploy creator first
    if(!creator && block->nchunks == 0)
        return 1;
    //already exist?
    uint32 i;
    for(i = 0; i < block->nchunks; i++){
        if(block->chunks[i] == nod)
            return 0;
    }
    //
    if(block->nchunks == 0){    //creator
        #define LEN 4 * 3
        uint32 *p = (uint32 *)cn_poke(nod->ffdn, LEN + 4);
        if(!p)
            return 1; //bad
        p[0] = LEN;
        p[1] = cwc_block_new;
        cn_wkr_request desc;
        desc.block.uid = block->uid;
        desc.block.size = block->size;
        cn_wkr_request_store(&desc, p + 2);
        #undef LEN
        cn_push(nod->ffdn);
        block->nchunks = 1;
        block->chunks[0] = nod;
        cnlog2("buf : block deploy (create); id:%d peer:%s\n", block->uid, nod->name);
        return 0;
    }
    //consumer
    //busy?
    if(block->dply_peer != 0 && block->dply_peer != nod)
        return 1;
    if(block->dply_peer == 0){
        block->dply_peer = nod;
        block->f_dply_create = 0;
        block->f_dply_recv = 0;
        block->f_dply_send = 0;
        block->f_prepare = 0;
        block->dply_send_handle = 0;
        block->dply_recv_handle = 0;
    }
    //create buffer
    if(block->dply_send_handle == 0)
        block->dply_send_handle = cn_peer_gethandle(block->chunks[0]);
    if(block->dply_recv_handle == 0)
        block->dply_recv_handle = cn_peer_gethandle(block->dply_peer);

    //invoke backend prepare
    if(!block->f_prepare && block->dply_send_handle != 0 && block->dply_recv_handle != 0){
        cn_handle_prep(block->dply_send_handle, block->dply_recv_handle);
        block->f_prepare = 1;
    }

    if(!block->f_dply_create){
        cnlog4("buf : deploy buf:%d to nod:%s - send create!\n", block->uid,
            nod->name);
        #define LEN CN_WKR_REQUEST_STORESIZE
        uint32 *p = (uint32 *)cn_poke(nod->ffdn, LEN + 8);
        if(p){
            p[0] = LEN + 4;
            p[1] = cwc_block_new;
            cn_wkr_request desc;
            desc.block.uid = block->uid;
            desc.block.size = block->size;
            cn_wkr_request_store(&desc, p + 2);
            cn_push(nod->ffdn);
            block->f_dply_create = 1;
        }
        #undef LEN
    }
    //transfer (sender)
    if(block->f_dply_create && !block->f_dply_send && block->f_prepare) {
        cnlog4("buf : deploy buf:%d to nod:%s - send transfer sender to nod:%s\n",
            block->uid, nod->name, block->chunks[0]->name);
        #define LEN CN_WKR_REQUEST_STORESIZE + CN_HANDLE_STORESIZE * 2
        uint32 *p = (uint32 *)cn_poke(block->chunks[0]->ffdn, LEN + 8);
        if(p){
            p[0] = LEN + 4;
            p[1] = cwc_transfer;
            cn_wkr_request desc;
            desc.trans.uid = block->uid;
            desc.trans.f_sender = 1;
            cn_wkr_request_store(&desc, p + 2);
            cn_handle_store(block->dply_send_handle,
                p + 2 + CN_WKR_REQUEST_STORESIZE / 4);
            cn_handle_store(block->dply_recv_handle,
                p + 2 + CN_WKR_REQUEST_STORESIZE / 4 + CN_HANDLE_STORESIZE / 4);
            cn_push(block->chunks[0]->ffdn);
            block->f_dply_send = 1;
        }
        #undef LEN
    }
    //transfer (receiver)
    if(block->f_dply_create && !block->f_dply_recv && block->f_prepare) {
        cnlog4("buf : deploy buf:%d to nod:%s - send transfer recvr to nod:%s\n",
            block->uid, nod->name, nod->name);
        #define LEN CN_WKR_REQUEST_STORESIZE + CN_HANDLE_STORESIZE * 2
        uint32 *p = (uint32 *)cn_poke(nod->ffdn, LEN + 8);
        if(p){
            p[0] = LEN + 4;
            p[1] = cwc_transfer;
            cn_wkr_request desc;
            desc.trans.uid = block->uid;
            desc.trans.f_sender = 0;
            cn_wkr_request_store(&desc, p + 2);
            cn_handle_store(block->dply_recv_handle,
                p + 2 + CN_WKR_REQUEST_STORESIZE / 4);
            cn_handle_store(block->dply_send_handle,
                p + 2 + CN_WKR_REQUEST_STORESIZE / 4 + CN_HANDLE_STORESIZE / 4);
            cn_push(nod->ffdn);
            block->f_dply_recv = 1;
        }
        #undef LEN
    }
    if(block->f_dply_send && block->f_dply_recv){
        block->chunks[block->nchunks] = block->dply_peer;
        block->nchunks++;
        block->dply_peer = 0;
        return 0;
    }else
        return 1;
}

void cn_block_use(cn_block_p block, cn_exec_p exec, uint8 create)
{
    if(create){
        if(block->creator)
            cnerr("block already has a creator\n");
        block->creator = exec;
    }
    if(exec->n_ios + 1 >= CN_EXEC_IOS_SLOTS)
        cnerr("cannot add block to exec - full\n");
    cn_iodesc_p io = exec->ios + exec->n_ios;
    io->io.block = block;
    io->f_channel = 0;
    io->f_sink = create;
    exec->n_ios += 1;
}

cn_block_p  _block_new(cn_buffer_desc_p desc)
{
    cnlog3("buf : new block...\n");
    cn_block_p block = cn_malloc(sizeof(struct cn_block_t));
    block->uid = desc->uid;
    block->creator = 0; //FIXME should this be set at creation?
    block->size = desc->size;
    block->ref = desc->nref;
    block->nchunks = 0;
    block->f_avail = 0;
    block->f_working = 0;
    cnlog2("buf : block created; uid:%d ref:%d @%#x(%#x)\n", block->uid, block->ref, block, sizeof(cn_block_p));
    return block;
}

void cn_block_unref(cn_block_p block)
{
    if(block->ref < 1)
        cnlog("buf : WARN - block ref count dropped below zero %d\n", block->uid);
        block->ref -= 1;
        if(block->ref == 0)
            cnlog3("buf : block %#x ready for destruction\n", block->uid);
        else
            cnlog3("buf : block %#x ref @ %d\n", block->uid);
}

/////////////////////////////////////////////
//
// CHANNEL
//

uint8 cn_channel_deploy(cn_channel_p chan)
{
    cn_peer_p send_peer, recv_peer;
    uint32 *p;

    if(chan->f_deployed)
        return 0;

    if(chan->sender.point.exec == 0 || chan->receiver.point.exec == 0)
        return 1;
    send_peer = chan->sender.point.exec->node;
    recv_peer = chan->receiver.point.exec->node;
    if(send_peer == 0 || recv_peer == 0)
        return 1;

    if(send_peer == recv_peer)
        cnerr("cannot make local channel\n");

    if(!chan->f_deploying){
        cnlog2("buf : channel deploying uid:%d\n", chan->uid);
        chan->f_deploying = 1;
    }

    if(chan->send_handle == 0)
        chan->send_handle = cn_peer_gethandle(send_peer);
    if(chan->recv_handle == 0)
        chan->recv_handle = cn_peer_gethandle(recv_peer);
    if(chan->send_handle == 0 || chan->recv_handle == 0)
        return 1;

    if(!chan->f_prepare){
        cn_handle_prep(chan->send_handle, chan->recv_handle);
        chan->f_prepare = 1;
    }

    //sender side
    if(!chan->f_sender_connect){
        #define LEN (CN_CHAN_DESC_STORESIZE + CN_HANDLE_STORESIZE * 2)
        p = cn_poke(send_peer->ffdn, LEN + 8);
        if(p){
            p[0] = LEN + 4;
            p[1] = cwc_channel;
            cn_chan_desc desc;
            desc.uid = chan->uid;
            desc.f_sender = 1;
            desc.size = chan->size;
            desc.maxacc = chan->maxacc;
            cn_chan_desc_store(&desc, p + 2);
            #if CANOE_LOGLEVEL >=2
                cnlog("buf : chan deploy; uid:%d rhandle:", chan->uid);
                cn_handle_log(chan->recv_handle);
                cnlog(" shandle:");
                cn_handle_log(chan->send_handle);
                cnlog("\n");
            #endif
            cn_handle_store(chan->recv_handle, p + 2 + (CN_CHAN_DESC_STORESIZE / 4));
            cn_handle_store(chan->send_handle,
                p + 2 + (CN_CHAN_DESC_STORESIZE / 4) + (CN_HANDLE_STORESIZE / 4));
            cn_push(send_peer->ffdn);
            chan->f_sender_connect = 1;
        }
        #undef LEN
    }

    //receiver side
    if(!chan->f_receiver_connect){
        #define LEN (CN_CHAN_DESC_STORESIZE + CN_HANDLE_STORESIZE * 2)
        p = cn_poke(recv_peer->ffdn, LEN + 8);
        if(p){
            p[0] = LEN + 4;
            p[1] = cwc_channel;
            cn_chan_desc desc;
            desc.uid = chan->uid;
            desc.size = chan->size;
            desc.f_sender = 0;
            desc.maxacc = chan->maxacc;
            cn_chan_desc_store(&desc, p + 2);
            cn_handle_store(chan->send_handle, p + 2 + CN_CHAN_DESC_STORESIZE / 4);
            cn_handle_store(chan->recv_handle,
                p + 2 + CN_CHAN_DESC_STORESIZE / 4 + CN_HANDLE_STORESIZE / 4);
            cn_push(recv_peer->ffdn);
            chan->f_receiver_connect = 1;
        }
        #undef LEN
    }

    if(chan->f_receiver_connect && chan->f_sender_connect){
        chan->f_deploying = 0;
        chan->f_deployed = 1;
        cnlog2("buf : deployed channel id:%d\n", chan->uid);
        return 0;
    }else
        return 1;
}

void cn_channel_use(cn_channel_p chan, cn_exec_p p, uint8 sink)
{
    if(sink){
        if(chan->sender.typ != cot_empty)
            cnerr("channel already has a sender - typ:%#x val:%#x\n", chan->sender.typ, chan->size);
        chan->sender.typ = cot_exec;
        chan->sender.point.exec = p;
        cnlog4("buf : channel:%#x used by exec:%#x (sender)\n", chan->uid, p->uid);
    }else{
        if(chan->receiver.typ != cot_empty)
            cnerr("channel already has a receiver\n");
        chan->receiver.typ = cot_exec;
        chan->receiver.point.exec = p;
        cnlog4("buf : channel:%#x used by exec:%#x (receiver)\n", chan->uid, p->uid);
    }
    cn_iodesc_p io = p->ios + p->n_ios;
    io->io.chan = chan;
    io->f_sink = sink;
    io->f_channel = 1;
    p->n_ios += 1;
}

cn_channel_p _channel_new(cn_buffer_desc_p desc)
{
    cn_channel_p p = cn_malloc(sizeof(struct cn_channel_t));
    cn_memset(p, 0, sizeof(struct cn_channel_t));
    p->uid = desc->uid;
    p->size = desc->size;
    p->maxacc = desc->maxacc;
    cnlog2("buf : channel created; size:%d maxacc:%d uid:%d @%#x (%#x)\n",
        p->size, p->maxacc, p->uid, p, sizeof(struct cn_channel_t));
    return p;
}
