#include "config_canoe.h"
#define CANOE_LOGLEVEL  CN_WORKER_LOGLEVEL
#include "canoe.h"

#ifndef CN_WORKER_CHUNK_SLOTS
#define CN_WORKER_CHUNK_SLOTS       40
#endif
#ifndef CN_EXEC_IO_SLOTS
#define CN_EXEC_IO_SLOTS           10
#endif
#ifndef CN_WORKER_TRANSFER_SLOTS
#define CN_WORKER_TRANSFER_SLOTS    10 
#endif
#ifndef CN_WORKER_FIFO_SLOTS
#define CN_WORKER_FIFO_SLOTS        10
#endif
#ifndef CN_WORKER_MAXRECUR
#define CN_WORKER_MAXRECUR          7
#endif

#define SLIDE(x)    (p + off); off += (x)

cn_worker * cn_worker_new(cn_io down, cn_io up)
{
    uint32 size = sizeof(cn_worker)
                + sizeof(cn_chunk *) * CN_WORKER_CHUNK_SLOTS
                + sizeof(cn_fifo_handle *) * CN_WORKER_FIFO_SLOTS
                + sizeof(cn_io) * CN_EXEC_IO_SLOTS
                + sizeof(cn_chunk *) * CN_EXEC_IO_SLOTS
                + sizeof(uint8) * CN_EXEC_IO_SLOTS
                + sizeof(cn_xfer) * CN_WORKER_TRANSFER_SLOTS
                + sizeof(cn_chunk *) * CN_WORKER_TRANSFER_SLOTS;
    void *p = cn_malloc(size);
    cn_memset(p, 0, size);
    uint32 off = 0;
    cn_worker *wkr = (cn_worker *)SLIDE(sizeof(cn_worker));
    wkr->io_down = down;
    wkr->io_up = up;
    wkr->block = (cn_chunk **)SLIDE(sizeof(cn_chunk *) * CN_WORKER_CHUNK_SLOTS);
    wkr->fifo = (cn_fifo_handle **)SLIDE(sizeof(cn_fifo_handle *) * CN_WORKER_FIFO_SLOTS);
    wkr->io = (cn_io *)SLIDE(sizeof(cn_io) * CN_EXEC_IO_SLOTS);
    wkr->io_chk = (cn_chunk **)SLIDE(sizeof(cn_chunk *) * CN_EXEC_IO_SLOTS);
    wkr->io_write = (uint8 *)SLIDE(sizeof(uint8) * CN_EXEC_IO_SLOTS);
    wkr->transfer = (cn_xfer *)SLIDE(sizeof(cn_xfer) * CN_WORKER_TRANSFER_SLOTS);
    wkr->transfer_chk = (cn_chunk **)SLIDE(sizeof(cn_chunk *) * CN_WORKER_TRANSFER_SLOTS);
    return wkr;
}

void            _wkr_recvblock(cn_worker *, void *data);
void            _wkr_recvchannel(cn_worker *, void *data);
void            _wkr_recvload(cn_worker *, void *data);
void            _wkr_recvhandlerequest(cn_worker *);
void            _wkr_sendhandle(cn_worker *wkr);
void            _wkr_recvexec(cn_worker *wkr, void *data);
void            _wkr_exec(cn_worker *wkr);
void            _wkr_load(cn_worker *wkr);
cn_fifo_handle *_wkr_getfifo_id(cn_worker *, cn_id);
void            _wkr_recvtransfer(cn_worker *wkr, void *data);
void            _wkr_transfers(cn_worker *wkr);
void            _wkr_recvblockdel(cn_worker *wkr, void *data);


void cn_worker_work(cn_worker *wkr)
{
    if(wkr->work_recur > CN_WORKER_MAXRECUR)
        cnerr("too much recursion in worker\n");
    wkr->work_recur += 1;
    //work on commands of the down pipe
    if(cn_receive(wkr->io_down, &wkr->msg)){
        uint32 *data = (uint32 *)wkr->msg.data;
        cn_wkrcmd cmd = data[0];
        switch(cmd){
            case cwc_invalid:
                cnerr("invalid command\n");
            case cwc_block_new:
                _wkr_recvblock(wkr, data + 1);
                break;
            case cwc_load:
                _wkr_recvload(wkr, data + 1);
                break;
            case cwc_handle_request:
                _wkr_recvhandlerequest(wkr);
                break;
            case cwc_exec:
                _wkr_recvexec(wkr, data + 1);
                break;
            case cwc_channel:
                _wkr_recvchannel(wkr, data + 1);
                break;
            case cwc_transfer:
                _wkr_recvtransfer(wkr, data + 1);
                break;
            case cwc_block_del:
                _wkr_recvblockdel(wkr, data + 1);
                break;
            default:
                #ifdef CHK
                    cn_msg_dump(&wkr->msg);
                #endif
                cnerr("wkr : unknown command; cmd:%d msglen:%d\n", cmd, wkr->msg.length);
        }
        cn_received(wkr->io_down);
    }
    //maintenance
    _wkr_sendhandle(wkr);
    _wkr_exec(wkr);
    _wkr_load(wkr);
    _wkr_transfers(wkr);
    //
    cn_wait();
    wkr->work_recur -= 1;
}

void _wkr_addchunk(cn_worker *wkr, cn_chunk *chk)
{
    uint32 i;
    for(i = 0; i < CN_WORKER_CHUNK_SLOTS; i++){
        if(wkr->block[i] == 0){
            wkr->block[i] = chk;
            return;
        }
    }
    cnerr("cannot add block - buffer full\n");
}

void _wkr_remchunk(cn_worker *wkr, cn_chunk *chk)
{
    uint32 i;
    for(i = 0; i < CN_WORKER_CHUNK_SLOTS; i++){
        if(wkr->block[i] == chk){
            wkr->block[i] = 0;
            return;
        }
    }
    cnerr("cannot remove block - not found\n");
}

void _wkr_addfifo(cn_worker *wkr, cn_fifo_handle *ff)
{
    uint32 i;
    for(i = 0; i < CN_WORKER_FIFO_SLOTS; i++){
        if(wkr->fifo[i] == 0){
            wkr->fifo[i] = ff;
            return;
        }
    }
    cnerr("cannot add block - buffer full\n");
}

void _wkr_recvblock(cn_worker *wkr, void *data)
{
    cn_chunk *chk = cn_chunk_new(data);
    _wkr_addchunk(wkr, chk);
}

void _wkr_recvblockdel(cn_worker *wkr, void *data)
{
    cn_wkr_request req;
    cn_wkr_request_load(&req, data);
    cn_chunk *chk = cn_worker_getchunk_id(wkr, req.block.uid);
    _wkr_remchunk(wkr, chk);
    cn_chunk_del(chk);
    cnlog3("wkr : removed chunk uid:%d\n", chk->uid);
}

void _wkr_recvchannel(cn_worker *wkr, void *data)
{
    cnlog3("wkr : received channel request\n");
    cn_fifo_handle *ff = cn_fifo_handle_new(data);
    _wkr_addfifo(wkr, ff);
    cnlog4("wkr : ...channel request done!\n");
}

void _wkr_recvload(cn_worker *wkr, void *data)
{
    cnlog2("wkr : received load request\n");
    cn_binary_desc_load(&wkr->load, data);
    wkr->f_load = 1;
}

void _wkr_add_transfer(cn_worker *wkr, cn_xfer xf, cn_chunk *chk)
{
    uint32 i;
    for(i = 0; i < CN_WORKER_TRANSFER_SLOTS; i++){
        if(wkr->transfer[i] == 0){
            wkr->transfer[i] = xf;
            wkr->transfer_chk[i] = chk;
            return;
        }
    }
    cnerr("cannot add transfer - buffer full\n");
}

cn_xfer _wkr_new_transfer(cn_worker *wkr, void *data)
{
    uint8 *d = data;
    cn_wkr_request req;
    uint32 off;
    off = cn_wkr_request_load(&req, d);
    cn_handle_t lhand, rhand;
    off += cn_handle_load(&lhand, d + off);
    off += cn_handle_load(&rhand, d + off);
    #if CANOE_LOGLEVEL >= 2
        cnlog("wkr : new transfer with l"); cn_handle_log(&lhand); cnlog(" and r");
        cn_handle_log(&rhand); cnlog(" send:%d\n", req.trans.f_sender);
    #endif
    cn_chunk *chk = cn_worker_getchunk_id(wkr, req.trans.uid);
    cn_xfer xf = cn_transfer(chk->buf, &lhand, &rhand, req.trans.f_sender);
    _wkr_add_transfer(wkr, xf, chk);
    if(!req.trans.f_sender || chk->f_valid)
        cn_transfer_start(xf);
    return xf;
}

void _wkr_recvtransfer(cn_worker *wkr, void *data)
{
    cnlog2("wkr : received transfer request\n");
    _wkr_new_transfer(wkr, data);
    cnlog3("wkr : /received transfer request\n");
}

void _wkr_recvhandlerequest(cn_worker *wkr)
{
    cnlog3("wkr : received handle request\n");
    //if(wkr->handlehook == 0)
    //    cnerr("no handlecall hooked - cannot aquire handle\n");
    if(wkr->f_sendhandle)
        cnerr("already in a handle request\n");
    wkr->f_sendhandle = 1;
}

void _wkr_sendhandle(cn_worker *wkr){
    if(!wkr->f_sendhandle)
        return;
    uint32 *d = cn_poke(wkr->io_up, 8 + CN_HANDLE_STORESIZE);
    if(d){
        cn_handle_p hand = cn_handle_alloc();
        d[0] = CN_HANDLE_STORESIZE + 4;
        d[1] = cmc_handle;
        cn_handle_store(hand, d + 2);
        cn_push(wkr->io_up);
        wkr->f_sendhandle = 0;
        cnlog3("wkr : sent handle ");
        #if CANOE_LOGLEVEL >= 3
            cn_handle_log(hand);
        #endif
        cnlog3("\n");
    }
}

void _wkr_recvexec(cn_worker *wkr, void *data){
    cnlog2("wkr : prepare exec\n");
    //receive description
    cn_run_desc desc;
    cn_run_desc_load(&desc, data);
    cn_runio_desc iodesc[desc.nios];
    desc.ios = iodesc;
    cn_run_desc_load_ios(&desc, data);
    //find ios
    uint32 i;
    for(i = 0; i < desc.nios; i++){
        cnlog3("wkr : looking for io %d/%d id:%d\n", i + 1, desc.nios, desc.ios[i].uid);
        cn_chunk * chk = cn_worker_getchunk_id(wkr, desc.ios[i].uid);
        if(chk){
            wkr->io[i] = cn_access(chk->buf, ~desc.ios[i].f_write);
            wkr->io_chk[i] = chk;
            wkr->io_write[i] = desc.ios[i].f_write;
            continue;
        }
        cn_fifo_handle * endp = _wkr_getfifo_id(wkr, desc.ios[i].uid);
        if(endp){
            wkr->io[i] = endp->fifo;
            wkr->io_chk[i] = 0;
            continue;
        }
        cnerr("wkr : cannot find io for exec\n");
    }
    wkr->io_n = desc.nios;
    wkr->f_exec = 1;
    wkr->exec_id = desc.uid;
}

void _wkr_exec(cn_worker *wkr)
{
    if(wkr->f_exec && !wkr->f_running){
        for(uint32 i = 0; i < wkr->io_n; i++){
            if(wkr->io_chk[i] && wkr->io_write[i] == 0 && wkr->io_chk[i]->f_valid == 0)
                return;
        }
        wkr->f_running = 1;
        cnlog("wkr : EXEC\n");
        cn_kern_setworker(wkr);
        cn_kern_run();
        cnlog("wkr : /EXEC\n");
        wkr->f_running = 0;
    }
    if(!wkr->f_running && wkr->exec_id){
        cnlog("wkr : send finish notice\n");
        uint32 *d = cn_poke(wkr->io_up, 4 + 3 * 4);
        if(d){
            cn_request_desc desc;
            desc.obj.uid = wkr->exec_id;
            d[0] = 3 * 4;
            d[1] = cmc_finish;
            cn_request_desc_store(&desc, d + 2);
            cn_push(wkr->io_up);
            wkr->exec_id = 0;
            wkr->f_exec = 0;
        }
    }
    for(uint32 i = 0; i < wkr->io_n; i++){
        if(wkr->io_chk[i])
            wkr->io_chk[i]->f_valid = 1;
    }
    //TODO free resources
    // free cn_access but not cn_fifo
}

void _wkr_load(cn_worker *wkr)
{
    if(!wkr->f_load)
        return;
    wkr->f_load = 0;
    cn_binary_exec(wkr->load.key, wkr->load.param);
    //shouldn't return
}

void _wkr_transfers(cn_worker *wkr)
{
    for(uint32 i = 0; i < CN_WORKER_TRANSFER_SLOTS; i++){
        if(wkr->transfer_chk[i] && wkr->transfer_chk[i]->f_valid){
            cn_transfer_start(wkr->transfer[i]);
            wkr->transfer_chk[i] = 0;
        }
        if(wkr->transfer[i] && cn_transfer_finished(wkr->transfer[i])){
            if(wkr->transfer_chk[i]){
                wkr->transfer_chk[i]->f_valid = 1;
                wkr->transfer_chk[i] = 0;
            }
            cn_transfer_del(wkr->transfer[i]);
            wkr->transfer[i] = 0;
        }
    }
}

cn_chunk * cn_worker_getchunk_id(cn_worker *wkr, cn_id uid)
{
    uint32 i;
    for(i = 0; i < CN_WORKER_CHUNK_SLOTS; i++){
        if(wkr->block[i] != 0 && wkr->block[i]->uid == uid)
            return wkr->block[i];
    }
    return 0;
}

cn_fifo_handle * _wkr_getfifo_id(cn_worker *wkr, cn_id uid)
{
    for(uint32 i = 0; i < CN_WORKER_FIFO_SLOTS; i++){
        if(wkr->fifo[i] != 0 && wkr->fifo[i]->uid == uid)
            return wkr->fifo[i];
    }
    return 0;
}

void cn_worker_clean(cn_worker *wkr)
{
    cnlog("TODO: clean up\n");
}

cn_chunk * cn_chunk_new(void *data)
{
    cn_wkr_request desc;
    cn_wkr_request_load(&desc, data);
    cn_chunk *chk = cn_malloc(sizeof(cn_chunk));
    chk->uid = desc.chunk.uid;
    chk->buf = cn_buffer(desc.chunk.size, 0);
    return chk;
}

void cn_chunk_del(cn_chunk *chk)
{
    cn_buffer_del(chk->buf);
    cn_free(chk);
}

cn_fifo_handle * cn_fifo_handle_new(void *data)
{
    uint32 *d = data;
    cn_chan_desc desc;
    cn_handle_t hand1, hand2;
    cn_chan_desc_load(&desc, data);
    cn_handle_load(&hand1, d + (CN_CHAN_DESC_STORESIZE / 4));
    cn_handle_load(&hand2, d + (CN_CHAN_DESC_STORESIZE / 4) + (CN_HANDLE_STORESIZE / 4));
    #if CANOE_LOGLEVEL >= 2
        cnlog("buf : new fifo rhandle:");
        cn_handle_log(&hand1);
        cnlog(" lhandler:");
        cn_handle_log(&hand2);
        cnlog(" size:%d maxacc:%d\n", desc.size, desc.maxacc);
    #endif
    cn_fifo_handle *ff = cn_malloc(sizeof(cn_fifo_handle));
    ff->uid = desc.uid;
    ff->fifo = cn_fifo(desc.size, desc.maxacc - 1, &hand2, &hand1, desc.f_sender);
    cnlog2("fifo: create @:%#x\n", ff);
    return ff;
}
