#include "config_canoe.h"
#define CANOE_LOGLEVEL  CN_SCHEDULING_LOGLEVEL
#include "canoe.h"

uint32 cnsched_lastnode = 1000;

void _exec_place(cn_exec_p task, cn_manager_p man)
{
    cn_peer_p peer;
    uint32 place;

    place = task->place;
    peer = man->peer[place];
    task->node = peer;
    cnlog3("schd: task %#x placed to peer #:%d name:%s\n", task->uid, place, peer->name);
}

/*void _exec_place(cn_exec_p task, cn_manager_p man)
{
    cn_peer_p p = 0;
    uint32 i;
    for(i = 0; i < 1000; i++){
        if(cnsched_lastnode >= cn_manager_peer_slots() - 1)
            cnsched_lastnode = 0;
        else
            cnsched_lastnode += 1;
        if(man->peer[cnsched_lastnode] != 0
        && man->peer[cnsched_lastnode]->f_worker){
            p = man->peer[cnsched_lastnode];
            break;
        }
    }
    if(p == 0)
        cnerr("cannot find worker node for task\n");
    task->node = p;
    cnlog3("schd: task %#x placed to peer:%s\n", task->uid, p->name);
}//*/


/*void cnman_canoe_place(cnman_canoe *cano, cnman_peer **nodes, uint32 nodes_s)
{
    uint32 node = 0;
    uint32 o, i;

    uint32 nproc = 0;
    for(i = 0; i < cano->obj_s; i++){
        if(cano->obj[i].typ == mot_exec && cano->obj[i].exec->f_process)
            nproc += 1;
    }

    uint32 nnodes = 0;
    for(i = 0; i < nodes_s; i++){
        if(nodes[i] && nodes[i]->f_worker)
            nnodes += 1;
    }

    if(nodes[0]->f_worker)
        cnerr("sched  : hacky condition needs to have a not worker as first node\n");

    cnlog("find mapping for %d proc on %d nodes\n", nproc, nnodes);
    if(nproc > nnodes)
        cnerr("this won't work\n");
    for(o = 0; o < cano->obj_s; o++){
        if(cano->obj[o].typ == mot_exec && cano->obj[o].exec->f_process){
            node += 1;
            while(nodes[node] == 0 || !nodes[node]->f_worker){
                node += 1;
                cnerr("node # exceeds nodes array\n");
            }
            cano->obj[o].exec->node = nodes[node];
            cnlog("sched  : map %#x to %#x\n", cano->obj[o].exec->uid, cn_adr2uint(nodes[node]->adr));
        }
    }
}*/
       

/*void cnman_canoe_place(cnman_canoe *cano, cnman_node **nodes, uint32 nodes_s)
{
    uint32 i, n, d;
    uint32 nproc = 0;
    for(i = 0; i < cano->obj_s; i++){
        if(cano->obj[i].typ == mot_process)
            nproc += 1;
    }
    sint32 imem[nodes_s], dmem[nodes_s];
    sint32 iproc[nproc], dproc[nproc];
    sint32 placed[nproc];
    uint32 proc[nproc];


    for(i = 0; i < nodes_s; i++){
        if(nodes[i] == 0){
            imem[i] = 0;
            dmem[i] = 0;
            continue;
        }
        imem[i] = nodes[i]->imem_free;
        dmem[i] = nodes[i]->dmem_free;
        cnlog("node #%d imem:%d dmem:%d\n", i, imem[i], dmem[i]);
    }
    for(i = 0, n = 0; i < cano->obj_s; i++){
        if(cano->obj[i].typ != mot_process)
            continue;
        proc[n] = i;
        placed[n] = -1;
        iproc[n] = cano->obj[i].proc->bin->imem;
        dproc[n] = cano->obj[i].proc->bin->dmem + HC_STACKSPACE;
        for(d = 0; d < cano->obj_s; d++){
            if(cano->obj[d].typ != mot_channel || (cano->obj[d].chan->sender.vs != cano->obj[i].vs && cano->obj[d].chan->receiver.vs != cano->obj[i].vs))
                continue;
            dproc[n] += cano->obj[d].chan->size; //FIXME balance and maxacc
        }
        cnlog("proc#%d imem:%d dmem:%d\n", n, iproc[n], dproc[n]);
        n += 1;
    }

    i = 0;
    while(i < nproc){
        placed[i] += 1;
        //move one layer up?
        if(placed[i] >= nodes_s){
            if(i == 0)
                cnerr("cannot find a mapping\n");
            placed[i] = -1;  //reset this layer
            i -= 1;         //move up
            //remove data from current layer
            imem[placed[i]] += iproc[i];
            dmem[placed[i]] += dproc[i];
            continue;
        }
        //can we place here
        if(iproc[i] <= imem[placed[i]] && dproc[i] <= dmem[placed[i]]){
            imem[placed[i]] -= iproc[i];
            dmem[placed[i]] -= dproc[i];
            i += 1;
        }
    }
    //commit
    for(i = 0; i < nproc; i++){
        cnman_process_place(cano->obj[proc[i]].proc, nodes[placed[i]]);
        cnlog("place %d -> %d\n", i, placed[i]);
    }
    cnlog1("cano %d placed\n", cano->uid);
}
*/
