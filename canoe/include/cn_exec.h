#ifndef CANOE_EXEC_HEADER
#define CANOE_EXEC_HEADER

#include "config_canoe.h"

//MANAGER FORWARD DECLARATION
struct cn_manager_t;
typedef struct cn_manager_t * cn_manager_p;

#define CN_EXEC_STORESIZE   20
#define CN_ASSIGN_STORESIZE 4

#ifndef CN_EXEC_IOS_SLOTS
#define CN_EXEC_IOS_SLOTS   8
#endif

struct cn_exec_desc_t {
    cn_id               uid;
    cn_id               core;
    uint32              n_assigns;
    uint8               f_noty_finished : 1;
    uint32              place;              //place to PE #
};
typedef struct cn_exec_desc_t * cn_exec_desc_p;

struct cn_assign_t{
    cn_id               buffer;
    uint8               f_sink : 1;     //write to the buffer
};
typedef struct cn_assign_t *    cn_assign_p;

struct cn_iodesc_t {
    uint8                   f_channel : 1;  //channel or block
    uint8                   f_sink : 1;     //access is write or read
    union {
        cn_channel_p        chan;
        cn_block_p          block;
    }           io;
};
typedef struct cn_iodesc_t * cn_iodesc_p;

struct cn_exec_t {
    cn_id                   uid;
    cn_id                   core;
    cn_binary_desc_p        bin;
    cn_peer_p               node;
    uint32                  n_ios;
    struct cn_iodesc_t      ios[CN_EXEC_IOS_SLOTS];
    //uint32                  f_task : 1;
    //uint32                  f_process : 1;
    uint32                  f_deploying : 1;
    uint32                  f_deployed  : 1;
    uint32                  f_bindeployed : 1;
    uint32                  f_iosdeployed : 1; //TODO change to ios deployed
    uint32                  f_finished : 1;
    uint32                  f_ioscleaned : 1;
    uint32                  f_peerpend : 1;
    uint32                  f_nodepend : 1;
    uint32                  f_noty_finished : 1;
    //uint32                  f_cacheios : 1;
    //uint32                  f_thread : 1;
    uint32                  place;              //pe # to place the exec
};
typedef struct cn_exec_t * cn_exec_p;

uint32      cn_exec_desc_store  (cn_exec_desc_p d, void *data);
uint32      cn_exec_desc_load   (cn_exec_desc_p d, void *data);
uint32      cn_assign_store     (cn_assign_p d, uint32 num, void *data);
uint32      cn_assign_load      (cn_assign_p d, uint32 pos, void *data);
cn_exec_p   cn_exec_new         (void *, cn_manager_p);
void        cn_exec_work        (cn_exec_p, cn_manager_p);
void        cn_exec_finish      (cn_exec_p);

#endif //CANOE_EXEC_HEADER