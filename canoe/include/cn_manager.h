#ifndef CN_MANAGER_HEADER
#define CN_MANAGER_HEADER

#include "config_canoe.h"
#include "cn_peer.h"
#include "cn_binary.h"
#include "cn_buffer.h"
#include "cn_base.h"
#include "cn_exec.h"

#ifdef __cplusplus
extern "C" {
#endif

//
/*struct man_ids_t {
    cn_id                   goi;
    cn_id                   cano;
    cn_id                   uid;
};
typedef struct man_ids_t    cnman_oid;*/

struct cn_object_t {
    enum cn_object_type_t   typ;
    union {
        cn_binary_desc_p    bin;
        cn_channel_p        chan;
        cn_exec_p           exec;
        cn_peer_p           peer;
        cn_block_p          block;
        void *              vs;
        cn_id *             id;
    }           obj;
};
typedef struct cn_object_t * cn_object_p;

struct cn_manager_t {
    cn_id                   uidsource;
    cn_object_p             obj; //CN_MANAGER_OBJECT_SLOTS
    cn_peer_p *             peer; //CN_MANAGER_PEER_SLOTS
};
typedef struct cn_manager_t * cn_manager_p;

cn_manager_p    cn_manager_new          ();
void            cn_manager_addpeer      (cn_manager_p, cn_peer_p);
void            cn_manager_work         (cn_manager_p);
void            cn_manager_addobj       (cn_manager_p, cn_object_p);
void            cn_manager_remobj_id    (cn_manager_p, cn_id);
cn_object_p     cn_manager_getobj_id    (cn_manager_p, cn_id);
cn_binary_desc_p cn_manager_getbin_core (cn_manager_p, cn_id);     
uint32          cn_manager_peer_slots   ();

void            cn_object_work          (cn_object_p, cn_manager_p);

cn_binary_desc_p cn_binary_new          (void *);

#ifdef __cplusplus
} //extern "C" {
#endif

//------------------------------------------------------------
/*void            cnman_manager_objectwork(cn_manager *man);
void            cnman_manager_peerwork  (cn_manager *man);
void            cnman_manager_handledata(cn_manager *man, cn_peer_p peer,
                                         uint32 cmd, void *data);
uint32          cnman_manager_objectspace(cn_manager *man);
void            cnman_manager_dump      (cn_manager *man, uint8 deep);
cnman_object *  cnman_manager_get       (cn_manager *, cn_id uid);

void            cnman_object_dump       (cnman_object *obj);

void            cnman_exec_deploybin    (cnman_exec *exc);
void            cnman_exec_deploy       (cnman_exec *exc);
void            cnman_exec_setfinished  (cnman_exec *exe);
void            cnman_exec_del          (cnman_exec *exc, cn_manager *man);
void            cnman_exec_findbin      (cnman_exec *exc, cn_manager *man);
cnman_exec *    cnman_exec_new          (cn_manager *man, void *);
void            cnman_exec_work         (cnman_exec *exe, cn_manager *man);

void            cnman_exec_checkdep     (cnman_exec *task);
void            cnman_exec_iosdeploy    (cnman_exec *task);
void            cnman_exec_iosclean     (cnman_exec *task);
void            cnman_exec_place        (cnman_exec *, cn_manager *);
void            cnman_exec_recvfin      (cn_manager *man, void *);

void            cnman_buffer_new        (cn_manager *man, void *data);

uint8           cnman_block_deploy(cnman_block *block, cn_peer_p nod, uint8 creator);
void            cnman_block_del(cnman_block *block, cn_manager *man);
void            cnman_block_work(cnman_block *block, cn_manager *man);
void            cnman_block_use(cnman_block *, cnman_exec *, uint8); //create

void            cnman_channel_use(cnman_channel *c, cnman_exec *p, uint8 sink);
uint8           cnman_channel_deploy(cnman_channel *c);

cnman_binary *  cnman_bin_new(cn_manager *man, void *);
uint8           cnman_bin_deploy(cnman_binary *bin, cn_peer_p node);*/

//------------------------------------------------------------
/*cn_handle_p     cnman_peer_gethandle(cn_peer_p);
void            cnman_peer_work(cn_peer_p, cn_manager *);
void            cnman_peer_dump(cn_peer_p, uint8 deep);*/

#endif
