#ifndef CN_WKRCMD_H
#define CN_WKRCMD_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
                    cwc_invalid = 0,
                    cwc_block_new,
                    cwc_block_del,
                    cwc_load,    //3
                    cwc_channel,
                    cwc_handle_request,
                    cwc_transfer,
                    cwc_exec,
}                   cn_wkrcmd;

typedef union{
    struct {
        uint32      p1;
        uint32      p2;
    }       gen;
    struct {
        uint32      uid;
        uint32      size;
    }       chunk;
    struct {
        uint32      uid;
        uint32      f_sender;
    }       trans;
    struct {
        uint32      uid;
        uint32      size;
    }       block;
/*        uint32      num;
        uint32      uid;
        uint32      num;
        uint32      size;
        uint32      f_sender;*/
}           cn_wkr_request;

typedef struct {
    uint32          uid;
    uint32          f_write;
}           cn_runio_desc;

typedef struct {
    uint32          uid;
    uint32          nios;
    uint32          f_noty_finish;
    cn_runio_desc * ios; 
}           cn_run_desc;

typedef struct {
    uint32          uid;
    uint32          size;
    uint32          maxacc;
    uint32          f_sender;
}           cn_chan_desc;

#define CN_WKR_REQUEST_STORESIZE    8
uint32  cn_wkr_request_store(cn_wkr_request *, void *);
uint32  cn_wkr_request_load (cn_wkr_request *, void *);
void    cn_run_desc_store(cn_run_desc *, void *);
void    cn_run_desc_load(cn_run_desc *, void *);
void    cn_run_desc_load_ios(cn_run_desc *, void *);
#define CN_CHAN_DESC_STORESIZE  16
void    cn_chan_desc_load(cn_chan_desc *, void *);
void    cn_chan_desc_store(cn_chan_desc *, void *);


#ifdef __cplusplus
} //extern "C" {
#endif

#endif
