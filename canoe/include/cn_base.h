#ifndef CN_BASE_HEADER
#define CN_BASE_HEADER

#include "basetypes.h"
#include "cn_backend.h"
#include "config_canoe.h"

#ifdef __cplusplus
extern "C" {
#endif

struct cn_msg_t {
    uint32          state;
    uint32          length;
    void *          data;
};

enum cn_object_type_t {
    cot_empty = 0,
    cot_binary,
    cot_channel,
    cot_canoe,
    cot_peer,
    cot_exec,
    cot_block,
    cot_buffer //virtual type describing block and channel
};

typedef uint32  cn_id;
typedef uint32  (*cn_call)();

void            cn_wait             ();

uint8           cn_receive          (cn_io, struct cn_msg_t *);
void            cn_received         (cn_io);
void            cn_msg_dump         (struct cn_msg_t *msg);

#ifdef __cplusplus
} //extern "C" {
#endif


#if CANOE_LOGLEVEL >= 1
    #define cnlog1 cnlog
#else
    #define cnlog1(...)
#endif
#if CANOE_LOGLEVEL >= 2
    #define cnlog2 cnlog
#else
    #define cnlog2(...)
#endif
#if CANOE_LOGLEVEL >= 3
    #define cnlog3 cnlog
#else
    #define cnlog3(...)
#endif
#if CANOE_LOGLEVEL >= 4
    #define cnlog4 cnlog
#else
    #define cnlog4(...)
#endif

#endif
