#ifndef CN_BUFFER_HEADER
#define CN_BUFFER_HEADER

#include "config_canoe.h"
#include "cn_peer.h"
#include "cn_base.h"
//#include "cn_exec.h"

#ifndef CN_BLOCK_CHUNK_SLOTS
#define CN_BLOCK_CHUNK_SLOTS    10
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct cn_manager_t;
struct cn_exec_t; //TODO remove when exec has own header
struct cn_object_t;

struct cn_buffer_desc_t {
    cn_id               uid;        //unique id
    uint32              size;       //size of fifo in bytes
    uint32              maxacc;     //size of the maximal access size
    uint32              nref;       //number of times to be used before automatic destruction
    uint8               f_block : 1;    //this is a block used for task feeding
    uint8               f_channel : 1;  //this is a channel used to connect processes
    void *              data;
};
typedef struct cn_buffer_desc_t * cn_buffer_desc_p;

struct cn_block_t {
    cn_id                   uid;
    struct cn_exec_t *      creator;
    uint32                  size;
    uint32                  ref;        //use counter till destruction
    uint32                  nchunks;    //number of referenced chunks
    cn_peer_p               chunks[CN_BLOCK_CHUNK_SLOTS];
    uint8                   f_avail : 1;
    uint8                   f_working : 1;
    uint8                   f_dply_create : 1;
    uint8                   f_dply_send : 1;
    uint8                   f_dply_recv : 1;
    uint8                   f_prepare : 1;
    cn_peer_p               dply_peer;
    cn_handle_p             dply_send_handle;
    cn_handle_p             dply_recv_handle;
};

typedef struct cn_block_t * cn_block_p;

struct cn_endpoint_t {
    enum cn_object_type_t   typ;
    union {
        struct cn_exec_t *  exec;
        cn_peer_p           peer;
    }           point;
};
typedef struct cn_endpoint_t * cn_endpoint_p;

struct cn_channel_t {
    cn_id                   uid;
    //cn_id                   src;
    //cn_id                   dst;
    uint32                  size;
    //uint32                  entry_s;
    //uint32                  fifo_s;
    uint32                  maxacc;
    sint8                   balance;
    struct cn_endpoint_t    sender;
    struct cn_endpoint_t    receiver;
    cn_handle_p             send_handle;
    cn_handle_p             recv_handle;
    uint8                   f_deploying : 1;
    uint8                   f_deployed : 1;
    uint8                   f_sender_buffer : 1;
    uint8                   f_receiver_buffer : 1;
    uint8                   f_sender_connect : 1;
    uint8                   f_receiver_connect : 1;
    uint8                   f_pulling : 1;
    uint8                   f_prepare : 1;
};
typedef struct cn_channel_t * cn_channel_p;

void            cn_buffer_new   (void *data, struct cn_object_t *dest);
void            cn_block_work   (cn_block_p, struct cn_manager_t *);
void            cn_block_use    (cn_block_p, struct cn_exec_t *, uint8); //param3: create
uint8           cn_block_deploy (cn_block_p, cn_peer_p, uint8); //param3: creator
void            cn_channel_use  (cn_channel_p, struct cn_exec_t *, uint8); //param3: sink
uint8           cn_channel_deploy(cn_channel_p);
void            cn_block_unref(cn_block_p block);

#ifdef __cplusplus
} //extern "C" {
#endif

#endif
