#ifndef CN_KERN_HEADER
#define CN_KERN_HEADER

#include "canoe.h"

#ifdef __cplusplus
extern "C" {
#endif

void    cn_kern_setworker   (cn_worker *);
void    cn_kern_setkernel   (cn_call);
void    cn_kern_run         ();
cn_io   cn_kern_getio       (uint32 num);
void    cn_kern_wait        ();

void    cn_kern_addbin      (cn_binary_desc_p);    //adds a binary
void    cn_kern_addbuffer   (cn_buffer_desc_p);    //adds a buffer 
void    cn_kern_addexec     (cn_exec_desc_p, cn_assign_p);
cn_io   cn_kern_ref         (cn_id, uint8); //uid, write
void    cn_kern_unref       (cn_id);        //uid

#ifdef __cplusplus
} //extern "C" {
#endif

#endif
