#ifndef HEAD_BASETYPES
#define HEAD_BASETYPES

#include <stdint.h>

typedef unsigned int        uint;
typedef signed char         sint8;
typedef uint8_t             uint8;
typedef int16_t             sint16;
typedef uint16_t            uint16;
typedef int32_t             sint32;
typedef uint32_t            uint32;
typedef int64_t             sint64;
typedef uint64_t            uint64;
typedef uint8_t             uint1;
typedef uintptr_t           uintptr;

typedef const char *        cstr;       //const string pointer
typedef char                strbuf;

#endif
