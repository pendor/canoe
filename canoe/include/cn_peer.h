#ifndef CN_PEER_HEADER
#define CN_PEER_HEADER

#include "config_canoe.h"
#include "cn_backend.h"
#include "cn_binary.h"
#include "cn_base.h"

#ifdef __cplusplus
extern "C" {
#endif

struct cn_manager_t;
struct cn_block_t;

struct cn_peer_t {
    cn_handle_p             handle;
    cn_io                   ffdn;           //fifo down: cm -> pe
    cn_io                   ffup;           //fifo up: pe -> cm
    struct cn_msg_t         ffup_msg;
    struct cn_block_t *     ref_req;        //read request
    uint32                  f_waithandle : 1;
    uint32                  f_worker : 1;
    cn_binary_desc_p        currbin;
    cstr                    name;
};

typedef struct cn_peer_t * cn_peer_p;

cn_peer_p       cn_peer_new             (cn_io dn, cn_io up);
void            cn_peer_work            (cn_peer_p, struct cn_manager_t *);
cn_handle_p     cn_peer_gethandle       (cn_peer_p);

#ifdef __cplusplus
} //extern "C" {
#endif

#endif
