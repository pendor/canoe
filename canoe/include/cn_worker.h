#ifndef CN_WORKER_HEADER
#define CN_WORKER_HEADER

#include "cn_base.h"
#include "cn_mancmd.h"
#include "config_canoe.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    cn_id                           uid;
    cn_buf                          buf;
    uint8                           f_valid : 1;
    uint8                           f_write : 1; //a ref is writing
}               cn_chunk;

typedef struct {
    cn_id                           uid;
    cn_io                           fifo;
}               cn_fifo_handle;

typedef struct {
    cn_io                           io_down;
    cn_io                           io_up;
    cn_chunk **                     block;  //CN_WORKER_CHUNK_SLOTS
    cn_fifo_handle **               fifo;   //CN_WORKER_FIFO_SLOTS
    cn_io *                         io;     //CN_EXEC_IO_SLOTS
    cn_chunk **                     io_chk; //CN_WORKER_IO_SLOTS
    uint8 *                         io_write; //CN_WORKER_IO_SLOTS
    uint32                          io_n;
    uint32                          exec_id;
    cn_xfer *                       transfer;//CN_WORKER_TRANSFER_SLOTS
    cn_chunk **                     transfer_chk;
    struct cn_msg_t                 msg;
    uint8                           f_sendhandle : 1;
    uint8                           f_exec : 1;
    uint8                           f_running : 1;
    uint8                           f_load : 1;
    uint32                          work_recur;
    struct cn_binary_desc_t         load;
}               cn_worker;

cn_worker *     cn_worker_new       (cn_io down, cn_io up);
void            cn_worker_work      (cn_worker *);
void            cn_worker_clean     (cn_worker *);
cn_chunk *      cn_worker_getchunk_id(cn_worker *, cn_id);
void            cn_worker_exec      (cn_worker *, cn_binary_desc_p);
cn_chunk *      cn_chunk_new        (void *);
void            cn_chunk_del        (cn_chunk *);
cn_fifo_handle * cn_fifo_handle_new (void *);


#ifdef __cplusplus
} //extern "C" {
#endif

#endif
