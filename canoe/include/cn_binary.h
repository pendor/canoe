#ifndef CN_BINARY_HEADER
#define CN_BINARY_HEADER

#include "config_canoe.h"
#include "basetypes.h"

#ifndef CN_BINARY_KEYLEN
#define CN_BINARY_KEYLEN 32
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct cn_peer_t;

struct cn_binary_desc_t {
    cn_id               uid;
    cn_id               core;
    uint32              typ;
    uint32              param;
    strbuf              key[CN_BINARY_KEYLEN];
};
typedef struct cn_binary_desc_t * cn_binary_desc_p;

#define             CN_BINARY_DESC_STORESIZE (16 + CN_BINARY_KEYLEN)
uint32              cn_binary_desc_store(cn_binary_desc_p d, void *data);
uint32              cn_binary_desc_load(cn_binary_desc_p d, void *data);

uint8               cn_binary_deploy(cn_binary_desc_p, struct cn_peer_t *);
cn_binary_desc_p    cn_binary_new(void *data);
void                cn_binary_keycpy(cn_binary_desc_p, cstr);

#ifdef __cplusplus
} //extern "C" {
#endif

#endif
