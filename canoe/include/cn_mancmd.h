#ifndef CN_MANCMD_H
#define CN_MANCMD_H 
#include "config_canoe.h"
#include "cn_base.h"
#include "basetypes.h"

#ifndef CN_BIN_ENVSLOTS
#define CN_BIN_ENVSLOTS 4
#endif

#ifdef __cplusplus
extern "C" {
#endif

enum cn_mancmd_t {
                        cmc_invalid = 0,
                        cmc_binary,
                        cmc_exec,
                        cmc_buffer,
                        cmc_request,
                        cmc_ref,        //5
                        cmc_unref,
                        cmc_uid,
                        cmc_handle,
                        cmc_finish
};
//#define CNBIN_XTENSA        0x1

//definition of a fifo between two processes
typedef union {
    struct {
        uint32          p1;
        uint32          p2;
    }           gen;    //generic access
    struct {
        uint32          buffer;
        uint32          f_write;
    }           ref;    //reference
    struct {
        uint32          uid;
        uint32          pad;
    }           obj;    //obj id (finished, ...)
}               cn_request_desc;


void    cn_buffer_desc_store(cn_buffer_desc_p d, void *data);
void    cn_buffer_desc_load(cn_buffer_desc_p d, void *data);
void    cn_request_desc_store(cn_request_desc *d, void *data);
void    cn_request_desc_load(cn_request_desc *d, void *data);

#ifdef __cplusplus
} //extern "C" {
#endif

#endif
