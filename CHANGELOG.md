# changelog
## v0.3.0
* the mapper in the CoreManager now honors predefined mappings from the process
  decription. To make the CoreManager use a predefined mapping every process in 
  the canoe has to have the mapping attribute set. To let the CoreManager render
  a mapping every process must have the mapping attribute set to 0. Setting only
  some mapping values will result in an error. 
## v0.2.1
* canoe placement honors process param size
* using the new functions from thdk 0.2.2 mem_size to report the correct available
  memory to the CoreManager.
* Using the new defines PE\_DMEM\_RESERVE and PE\_IMEM\_RESERVE to keep some memory
  not clobbered by the canoe placer. This is usefull for overhead information that
  is not concidered by the placer.
## v0.2.0
* libapp - this is a thdk library that will define the whole canoe os for the 
  application core just leaving out one function: void run(ffptr) that is supposed
  to define and start some canoe networks. The argument given is a fifo pointer
  to the coremanager that is waiting for a KPN description on this channel
* canoe channels are now alloed to be pure local, that is connecting two processes
  that live on the same PE. So no network traffic is generated using local channels
* for the first time the mapping algorithm is somehow intelligent. It considers
  all given processes at once trying to find a solution where all proccsses
  are placed.
* multitasking on PEs. The PEs OS layer of cacnoe is now able to handle multiple
  processes and switch between them in a cooperative fashion.
* the project file config_canoe is now optional. The defines that were used to live
  in this file are now defined in the headerfiles. It is still possible to change
  settings from the canoe_config.h since the header will not overwrite values
## v0.1.1
* minor bug fix
## v0.1.0
* first release